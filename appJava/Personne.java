public class Personne {
    private int idP;
    private String nomP;
    private String prenomP;
    private int age;
    private double poids;
    private String numTel;

    public Personne(int idP, String nomP, String prenomP, int age, double poids, String numTel) {
        this.idP = idP;
        this.nomP = nomP;
        this.prenomP = prenomP;
        this.age = age;
        this.poids = poids;
        this.numTel = numTel;
    }

    public int getIdP() {
        return idP;
    }
    public void setIdP(int idP) {
        this.idP = idP;
    }
    public String getNomP() {
        return nomP;
    }
    public void setNomP(String nomP) {
        this.nomP = nomP;
    }
    public String getPrenomP() {
        return prenomP;
    }
    public void setPrenomP(String prenomP) {
        this.prenomP = prenomP;
    }
    public int getAge() {
        return age;
    }
    public void setAge(int age) {
        this.age = age;
    }
    public double getPoids() {
        return poids;
    }
    public void setPoids(double poids) {
        this.poids = poids;
    }
    public String getNumTel() {
        return numTel;
    }
    public void setNumTel(String numTel) {
        this.numTel = numTel;
    }

    @Override
    public String toString(){
        return "id "+this.idP+ " " + prenomP+" "+nomP+" a "+age+" ans, il pese "+poids+" kg son tel est "+numTel;
    }
    

}


