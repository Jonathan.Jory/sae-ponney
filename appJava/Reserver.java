import java.sql.Time;
import java.util.Date;

public class Reserver {
    private Time heureD;
    private Date dateD;
    private int idC;
    private int idPo;
    private int idCl;

    public Reserver(Time heureD, Date dateD, int idC, int idPo, int idCl) {
        this.heureD = heureD;
        this.dateD = dateD;
        this.idC = idC;
        this.idPo = idPo;
        this.idCl = idCl;
    }
    
    public Time getHeureD() {
        return heureD;
    }
    public void setHeureD(Time heureD) {
        this.heureD = heureD;
    }
    public Date getDateD() {
        return dateD;
    }
    public void setDateD(Date dateD) {
        this.dateD = dateD;
    }
    public int getIdC() {
        return idC;
    }
    public void setIdC(int idC) {
        this.idC = idC;
    }
    public int getIdPo() {
        return idPo;
    }
    public void setIdPo(int idPo) {
        this.idPo = idPo;
    }
    public int getIdCl() {
        return idCl;
    }
    public void setIdCl(int idCl) {
        this.idCl = idCl;
    }

    public String toString(){
        return "Le client "+idCl+" est affecter au cours "+idC+" a "+heureD+" le "+dateD+" avec le poney "+idPo;
    }
}

