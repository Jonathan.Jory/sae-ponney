import java.sql.Time;

public class Cours {
    private int idC;
    private int idM;
    private String nomC;
    private String typeC;
    private String descriptionC;
    private Time duree;

    public Cours(int idC, int idM, String nomC, String typeC, String descriptionC, Time duree) {
        this.idC = idC;
        this.idM = idM;
        this.nomC = nomC;
        this.typeC = typeC;
        this.descriptionC = descriptionC;
        this.duree = duree;
    }

    public int getIdC() {
        return idC;
    }

    public void setIdC(int idC) {
        this.idC = idC;
    }

    public int getIdM() {
        return idM;
    }

    public void setIdM(int idM) {
        this.idM = idM;
    }

    public String getNomC() {
        return nomC;
    }

    public void setNomC(String nomC) {
        this.nomC = nomC;
    }

    public String getTypeC() {
        return typeC;
    }

    public void setTypeC(String typeC) {
        this.typeC = typeC;
    }

    public String getDescriptionC() {
        return descriptionC;
    }

    public void setDescriptionC(String descriptionC) {
        this.descriptionC = descriptionC;
    }

    public Time getDuree() {
        return duree;
    }

    public void setDuree(Time duree) {
        this.duree = duree;
    }

    @Override
    public String toString(){
        return "Le cours "+idC+" a pour moniteur l'id "+idM+", le cours est de type "+typeC+" il dure "+duree + " et on y fait "+descriptionC;
    }
}



