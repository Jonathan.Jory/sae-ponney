from .app import app
from flask import render_template, request, url_for , redirect, jsonify
from flask_wtf import FlaskForm
from wtforms import StringField , HiddenField, FloatField, BooleanField, validators, SelectField, TextAreaField, TimeField, DateField, ValidationError
from wtforms.validators import DataRequired
from flask_login import login_user , current_user,logout_user,login_required

from wtforms import PasswordField
from hashlib import sha256

from .models import *

from datetime import timedelta, datetime

DICO_JOURS_SEMAINE ={0: "Lundi", 1: "Mardi",2: "Mercredi",3: "Jeudi",4: "Vendredi",5: "Samedi",6: "Dimanche"}

@app.route("/")
def accueil():
    poneys = get_poney()
    return render_template(
        "accueil.html", poneys=poneys)

@app.route('/login/', methods = ["GET", "POST"])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('logout'))
    if request.method == "POST":
        name = request.form["name"]
        mdp = request.form["password"]
        personne = get_personne_email_mdp(name, mdp)
        if personne is not None:
            if est_moniteur(personne.id):
                moniteur = get_moniteur_by_id(personne.id)
                login_user(moniteur)
                return redirect(url_for("accueil"))
            else:
                client = get_client_by_id(personne.id)
                login_user(client)
                return redirect(url_for('accueil'))
        return render_template('login.html')
    return render_template('login.html')

@app.route("/logout/")
@login_required
def logout():
    logout_user()
    return redirect(url_for("login"))


@app.route("/menu/moniteur")
@login_required
def menu_moniteur():
    if not current_user.est_moniteur():
        return redirect(url_for('accueil'))    
    return render_template(
        "menu_moniteur.html",)


@app.route("/poney/<id>")
def detail_poney(id):
    poney = get_poney_by_id(id)
    return render_template("poney.html", poney=poney)

@app.route("/client/<id>")
@login_required
def detail_client(id):
    if not current_user.est_moniteur():
        return redirect(url_for('accueil')) 
    client = get_client_by_id(id)
    return render_template("client.html", client=client)

@app.route("/moniteur/<id>")
@login_required
def detail_moniteur(id):
    if not current_user.est_moniteur():
        return redirect(url_for('accueil')) 
    moniteur = get_moniteur_by_id(id)
    return render_template("moniteur.html", moniteur=moniteur)

@app.route("/paiement/<id>")
@login_required
def detail_paiement(id):
    if not current_user.est_moniteur():
        if int(id) != current_user.id:
            return redirect(url_for('accueil')) 
    paiement = get_paiement_by_id(id)
    return render_template("paiement.html", paiement=paiement)

@app.route("/cour/<id>")
@login_required
def detail_cour(id):
    cour = get_cours_by_id(id)
    moniteur = get_moniteur_by_id(cour.id_m)
    return render_template("cour.html", cour=cour, moniteur=moniteur)

@app.route("/reservation/<date_c>/<idc>/<idcl>")
@login_required
def detail_reservation(date_c, idc, idcl):
    if not current_user.est_moniteur():
        if int(idcl) != current_user.id:
            return redirect(url_for('accueil')) 
    reservation = get_reservation_by_date_cours_client(date_c, idc, idcl)
    client = get_client_by_id(idcl)
    cour = get_cours_by_id(idc)
    moniteur = get_moniteur_by_id(cour.id_m)
    poney = get_poney_by_id(reservation.idpo)
    return render_template("reservation.html", reservation=reservation, client=client, cour=cour, moniteur=moniteur, poney =poney)


#Poney ------------------------------------------------------------------------------


class PoneyForm(FlaskForm):
    idpo = HiddenField ("idpo")
    nom_poney = StringField("nom_poney", validators =[DataRequired()])
    poidssup = FloatField("poidssup", validators =[DataRequired()])
    race = StringField("race", validators =[DataRequired()])
    robe = StringField("robe", validators =[DataRequired()])
    def get_form_field(self):
        return ['nom_poney', 'poidssup', 'race', 'robe']

class PoneyFormFiltre(FlaskForm):
    nom_poney_filtre = StringField("nom_poney")
    poidssup_filtre = FloatField("poidssup")
    race_filtre = StringField("race")
    robe_filtre = StringField("robe")
    def get_form_field(self):
        return ['nom_poney_filtre', 'poidssup_filtre', 'race_filtre', 'robe_filtre']

@app.route('/api/data/poney/', methods = ["POST"])
@login_required
def data_poney():
    nom_poney = request.form["nom_poney_filtre"]
    poidssup = request.form["poidssup_filtre"]
    race = request.form["race_filtre"]
    robe = request.form["robe_filtre"]
    query = get_poney_avec_filtre(nom_poney, poidssup, race, robe)
    # search filter
    search = request.args.get('search[value]')
    if search:
        query = recherche_poney(query, search)
    total_filtered = query.count()

    # sorting
    order = []
    i = 0
    while True:
        col_index = request.args.get(f'order[{i}][column]')
        if col_index is None:
            break
        col_name = request.args.get(f'columns[{col_index}][data]')
        descending = request.args.get(f'order[{i}][dir]') == 'desc'
        col = get_attribut_poney(col_name)
        if descending:
            col = col.desc()
        order.append(col)
        i += 1
    if order:
        query = query.order_by(*order)


    # pagination
    start = request.args.get('start', type=int)
    length = request.args.get('length', type=int)
    query = query.offset(start).limit(length)
    # response
    return {
        'data': [poney.to_dict() for poney in query],
        'recordsFiltered': total_filtered,
        'recordsTotal': poney_count(),
        'draw': request.args.get('draw', type=int),
    }

@app.route('/voirPoney/')
@login_required
def index_poney():
    f = PoneyForm()
    ff= PoneyFormFiltre()
    print(ff.get_form_field())
    return render_template('ajax_table_poney.html', title='Page Poney', form=f, form_filtre=ff)


@app.route('/delete_poney/',methods=['POST'])
@login_required
def delete_poney():
    delete_un_poney_bd(request.form["id"])
    return ""

@app.route('/update_poney_options', methods=['POST'])
@login_required
def update_poney_options():
    client_id = request.form['client_id']
    client = get_client_by_id(client_id)
    cours_id = request.form['cours_id']
    cours = get_cours_by_id(cours_id)
    date_reservation = datetime.strptime(request.form['date_reservation'], "%Y-%m-%d")
    datetime_reservation = datetime.combine(date_reservation, cours.heure_debut)
    poneys = get_list_valid_poney_reservation(client, datetime_reservation, cours.duree)
    if not poneys:
        return jsonify([])
    else:
        return jsonify([{'id': poney.idpo, 'name':poney.nom_poney, 'poids': poney.poidssup} for poney in poneys])


@app.route("/save/poney/", methods =("POST",))
@login_required
def save_poney():
    f = PoneyForm()
    if f.validate_on_submit():
        o = Poney(nom_poney=f.nom_poney.data, poidssup= f.poidssup.data, race= f.race.data, robe= f.robe.data)
        o.image = "poneyShetland2.jpg"
        db.session.add(o)
        db.session.commit()
        return redirect(url_for("index_poney"))
    ff = PoneyFormFiltre()
    return render_template("ajax_table_poney.html", form=f, open_modal=True, form_filtre = ff)

@app.route("/edit/poney/", methods =("POST",))
@login_required
def edit_poney():
    f = PoneyForm()
    if f.validate_on_submit():
        poney = get_poney_by_id(f.idpo.data)    
        poney.nom_poney = f.nom_poney.data
        poney.poidssup = f.poidssup.data 
        poney.race = f.race.data 
        poney.robe = f.robe.data 
        db.session.add(poney)
        db.session.commit()
        return redirect(url_for("index_poney"))
    ff = PoneyFormFiltre()
    return render_template("ajax_table_poney.html", form=f, open_modal_edit=True, form_filtre = ff)


#Moniteur ------------------------------------------------------------------------

class MoniteurForm(FlaskForm):
    def num_tel_valide(form, field):
        if(len(field.data) != 10):
            raise ValidationError("Le numéro de tel n'est pas valide.(10 chiffres)")
    id = HiddenField ("id")
    nom_personne = StringField("nom_personne", validators =[DataRequired()])
    prenom_personne = StringField("prenom_personne", validators =[DataRequired()])
    age_personne = FloatField("age_personne", validators =[DataRequired()])
    poids = FloatField("poids", validators =[DataRequired()])
    num_tel = StringField("num_tel", validators =[DataRequired(), num_tel_valide])
    def get_form_field(self):
        return ['nom_personne', 'prenom_personne', 'age_personne', 'poids', 'num_tel']
    
class MoniteurFormFiltre(FlaskForm):
    nom_personne_filtre = StringField("nom_personne")
    prenom_personne_filtre = StringField("prenom_personne")
    age_personne_filtre = FloatField("age_personne")
    poids_filtre = FloatField("poids")
    num_tel_filtre = StringField("num_tel")
    def get_form_field(self):
        return ['nom_personne_filtre', 'prenom_personne_filtre', 'age_personne_filtre', 'poids_filtre', 'num_tel_filtre']

@app.route('/api/data/moniteur/', methods = ["POST"])
@login_required
def data_moniteur():
    nom = request.form["nom_personne_filtre"]
    prenom = request.form["prenom_personne_filtre"]
    age = request.form["age_personne_filtre"]
    poids = request.form["poids_filtre"]
    num_tel = request.form["num_tel_filtre"]
    query = get_moniteur_avec_filtre(nom, prenom, age, poids, num_tel)

    # search filter
    search = request.args.get('search[value]')
    if search:
        query = recherche_moniteur(query, search)
    total_filtered = query.count()

    # sorting
    order = []
    i = 0
    while True:
        col_index = request.args.get(f'order[{i}][column]')
        if col_index is None:
            break
        col_name = request.args.get(f'columns[{col_index}][data]')
        descending = request.args.get(f'order[{i}][dir]') == 'desc'
        print("col_name ",col_name)
        col = getattr(Moniteur, col_name)
        print(type)
        print("col ",col)
        if descending:
            col = col.desc()
        order.append(col)
        i += 1
    if order:
        query = query.order_by(*order)


    # pagination
    start = request.args.get('start', type=int)
    length = request.args.get('length', type=int)
    query = query.offset(start).limit(length)
    # response
    return {
        'data': [moniteur.to_dict() for moniteur in query],
        'recordsFiltered': total_filtered,
        'recordsTotal': moniteur_count(),
        'draw': request.args.get('draw', type=int),
    }

@app.route('/voirMoniteur/')
@login_required
def index_moniteur():
    f = MoniteurForm()
    ff = MoniteurFormFiltre()
    return render_template('ajax_table_moniteur.html', title='Page Moniteur', form=f, form_filtre = ff)

@app.route('/delete_moniteur/',methods=['POST'])
@login_required
def delete_moniteur():
    delete_un_moniteur_bd(request.form["id"])
    return ""
    

@app.route("/save/moniteur/", methods =("POST",))
@login_required
def save_moniteur():
    f = MoniteurForm()
    if f.validate_on_submit():
        o = Personne(nom_personne=f.nom_personne.data, prenom_personne=f.prenom_personne.data, age_personne=f.age_personne.data, poids=f.poids.data, num_tel=f.num_tel.data)
        o.password = "mdp"
        db.session.add(o)
        db.session.commit()
        m = Moniteur(id=o.id)
        db.session.add(m)
        db.session.commit()
        return redirect(url_for("index_moniteur"))
    ff = MoniteurFormFiltre()
    return render_template("ajax_table_moniteur.html", form=f, open_modal=True, form_filtre = ff)

@app.route("/edit/moniteur/", methods =("POST",))
@login_required
def edit_moniteur():
    f = MoniteurForm()
    if f.validate_on_submit():
        moniteur = get_moniteur_by_id(f.id.data)    
        moniteur.personne.nom_personne = f.nom_personne.data
        moniteur.personne.prenom_personne = f.prenom_personne.data 
        moniteur.personne.age_personne = f.age_personne.data 
        moniteur.personne.poids = f.poids.data 
        moniteur.personne.num_tel = f.num_tel.data 
        db.session.add(moniteur)
        db.session.commit()
        return redirect(url_for("index_moniteur"))
    ff = MoniteurFormFiltre()
    return render_template("ajax_table_moniteur.html", form=f, open_modal_edit=True, form_filtre = ff)


#Client ------------------------------------------------------------------------


class ClientForm(FlaskForm):
    def num_tel_valide(form, field):
        if(len(field.data) != 10):
            raise ValidationError("Le numéro de tel n'est pas valide.(10 chiffres)")
    id = HiddenField ("id")
    nom_personne = StringField("nom_personne", validators =[DataRequired()])
    prenom_personne = StringField("prenom_personne", validators =[DataRequired()])
    age_personne = FloatField("age_personne", validators =[DataRequired()])
    poids = FloatField("poids", validators =[DataRequired()])
    num_tel = StringField("num_tel", validators =[DataRequired(), num_tel_valide])
    cotisation = BooleanField("cotisation", default=False, validators=[validators.AnyOf([True, False])])
    def get_form_field(self):
        return ['nom_personne', 'prenom_personne', 'age_personne', 'poids', 'num_tel', 'cotisation']

class ClientFormFiltre(FlaskForm):
    nom_personne_filtre = StringField("nom_personne")
    prenom_personne_filtre = StringField("prenom_personne")
    age_personne_filtre = FloatField("age_personne")
    poids_filtre = FloatField("poids")
    num_tel_filtre = StringField("num_tel")
    cotisation_filtre = StringField("Cotisation (oui/non/rien")
    def get_form_field(self):
        return ['nom_personne_filtre', 'prenom_personne_filtre', 'age_personne_filtre', 'poids_filtre', 'num_tel_filtre', 'cotisation_filtre']


@app.route('/api/data/client/', methods = ["POST"])
@login_required
def data_client():
    nom = request.form["nom_personne_filtre"]
    prenom = request.form["prenom_personne_filtre"]
    age = request.form["age_personne_filtre"]
    poids = request.form["poids_filtre"]
    num_tel = request.form["num_tel_filtre"]
    cotisation = request.form["cotisation_filtre"]
    query = get_client_avec_filtre(nom, prenom, age, poids, num_tel, cotisation)

    # search filter
    search = request.args.get('search[value]')
    if search:
        query = recherche_client(query, search)
    total_filtered = query.count()

    # sorting
    order = []
    i = 0
    while True:
        col_index = request.args.get(f'order[{i}][column]')
        if col_index is None:
            break
        col_name = request.args.get(f'columns[{col_index}][data]')
        descending = request.args.get(f'order[{i}][dir]') == 'desc'
        col = get_attribut_client(col_name)
        if descending:
            col = col.desc()
        order.append(col)
        i += 1
    if order:
        query = query.order_by(*order)


    # pagination
    start = request.args.get('start', type=int)
    length = request.args.get('length', type=int)
    query = query.offset(start).limit(length)
    # response
    return {
        'data': [client.to_dict() for client in query],
        'recordsFiltered': total_filtered,
        'recordsTotal': client_count(),
        'draw': request.args.get('draw', type=int),
    }

@app.route('/voirClient/')
@login_required
def index_client():
    f = ClientForm()
    ff = ClientFormFiltre()
    return render_template('ajax_table_client.html', title='Page Client', form=f, form_filtre = ff)

@app.route('/delete_client/',methods=['POST'])
@login_required
def delete_client():
    delete_un_client_bd(request.form["id"])
    return ""
    

@app.route("/save/client/", methods =("POST",))
@login_required
def save_client():
    f = ClientForm()
    if f.validate_on_submit():
        o = Personne(nom_personne=f.nom_personne.data, prenom_personne=f.prenom_personne.data, age_personne=f.age_personne.data, poids=f.poids.data, num_tel=f.num_tel.data)
        o.password = "mdp"
        db.session.add(o)
        db.session.commit()
        m = Client(id=o.id, cotisation=f.cotisation.data)
        db.session.add(m)
        db.session.commit()
        return redirect(url_for("index_client"))
    ff = ClientFormFiltre()
    return render_template("ajax_table_client.html", form=f, open_modal=True, form_filtre = ff)

@app.route("/edit/client/", methods =("POST",))
@login_required
def edit_client():
    f = ClientForm()
    if f.validate_on_submit():
        client = get_client_by_id(f.id.data)    
        client.personne.nom_personne = f.nom_personne.data
        client.personne.prenom_personne = f.prenom_personne.data 
        client.personne.age_personne = f.age_personne.data 
        client.personne.poids = f.poids.data 
        client.personne.num_tel = f.num_tel.data 
        client.cotisation = f.cotisation.data 
        db.session.add(client)
        db.session.commit()
        return redirect(url_for("index_client"))
    ff = ClientFormFiltre()
    return render_template("ajax_table_client.html", form=f, open_modal_edit=True, form_filtre = ff)


#Cours ------------------------------------------------------------------------


class CoursForm(FlaskForm):
    def moniteur_dispo(form, field):
        if not is_dispo_moniteur(field.data, form.jours_semaine.data, form.heure_debut_cours.data, form.duree_cours.data):
            raise ValidationError("Le moniteur n'est pas disponnible sur ce créneau")

    def duree_superieur_2_heure(form, field):
        if int(field.data.hour) == 2 and int(field.data.minute) > 0:
            raise ValidationError("le cours est trop long")
        elif int(field.data.hour) > 2:
            raise ValidationError("le cours est trop long")


    choix_jours_de_la_semaine = ["Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi", "Dimanche"]
    choix_type_cours = ["Individuel", "Collectif"]
    id_c = HiddenField ("id")
    nom_cours = StringField("nom_cours", validators =[DataRequired()])
    description_cours = TextAreaField("description_cours", validators =[DataRequired()])
    type_cours = SelectField("type_cours", choices=choix_type_cours, validators =[DataRequired()])
    jours_semaine = SelectField("jours_semaine", choices=choix_jours_de_la_semaine, validators =[DataRequired()])
    heure_debut_cours = TimeField("heure_debut", format='%H:%M', validators =[DataRequired()])
    duree_cours = TimeField("duree_cours", format='%H:%M', validators =[DataRequired(), duree_superieur_2_heure])
    
    prenom_moniteur = SelectField("prenom_moniteur", choices=[],validators =[DataRequired(),moniteur_dispo])
    def get_form_field(self):
        return ['nom_cours', 'description_cours', 'type_cours', 'jours_semaine', 'heure_debut_cours', 'duree_cours','prenom_moniteur']
    def remplir_formulaire(self):
        liste_moniteur = []
        for moniteur in get_moniteur():
            liste_moniteur.append((moniteur.id, "id " + str(moniteur.id) + ": " +moniteur.personne.prenom_personne+" "+moniteur.personne.nom_personne))
        self.prenom_moniteur.choices = liste_moniteur


class CoursFormFiltre(FlaskForm):
    choix_jours_de_la_semaine = ["Aucun Filtre", "Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi", "Dimanche"]
    choix_type_cours = ["Aucun Filtre", "Individuel", "Collectif"]

    nom_cours_filtre = StringField("nom_cours")
    type_cours_filtre = SelectField("type_cours", choices=choix_type_cours)
    jours_semaine_filtre = SelectField("jours_semaine", choices=choix_jours_de_la_semaine)
    heure_debut_cours_filtre = TimeField("heure_debut", format='%H:%M')
    duree_cours_filtre = TimeField("duree_cours", format='%H:%M')  
    prenom_moniteur_filtre = SelectField("prenom_moniteur", choices=[])

    def get_form_field(self):
        return ['nom_cours_filtre', 'type_cours_filtre', 'jours_semaine_filtre', 'heure_debut_cours_filtre', 'duree_cours_filtre', 'prenom_moniteur_filtre']
    def remplir_formulaire(self):
        liste_moniteur = [("None", "Aucun filtre")]
        for moniteur in get_moniteur():
            liste_moniteur.append((moniteur.id, "id " + str(moniteur.id) + ": " +moniteur.personne.prenom_personne+" "+moniteur.personne.nom_personne))
        self.prenom_moniteur_filtre.choices = liste_moniteur

@app.route('/api/data/cours/', methods = ["GET","POST"])
@login_required
def data_cours():
    nom_cours = request.form["nom_cours_filtre"]
    type_cours = request.form["type_cours_filtre"]
    jours_semaine = request.form["jours_semaine_filtre"]
    heure_debut = request.form["heure_debut_cours_filtre"]
    duree_cours = request.form["duree_cours_filtre"]
    id_moniteur = request.form["prenom_moniteur_filtre"]

    query = get_cours_avec_filtre(nom_cours, type_cours, jours_semaine, heure_debut, duree_cours, id_moniteur)

    # search filter
    search = request.args.get('search[value]')
    if search:
        query = recherche_cours(query, search)
    total_filtered = query.count()

    # sorting
    order = []
    i = 0
    while True:
        col_index = request.args.get(f'order[{i}][column]')
        if col_index is None:
            break
        col_name = request.args.get(f'columns[{col_index}][data]')
        descending = request.args.get(f'order[{i}][dir]') == 'desc'
        col = get_attribut_cours(col_name)
        if descending:
            col = col.desc()
        order.append(col)
        i += 1
    if order:
        query = query.order_by(*order)


    # pagination
    start = request.args.get('start', type=int)
    length = request.args.get('length', type=int)
    query = query.offset(start).limit(length)
    # response
    return {
        'data': [cours.to_dict() for cours in query],
        'recordsFiltered': total_filtered,
        'recordsTotal': cours_count(),
        'draw': request.args.get('draw', type=int),
    }

@app.route('/voirCours/')
@login_required
def index_cours():
    f = CoursForm()
    f.remplir_formulaire()
    ff = CoursFormFiltre()
    ff.remplir_formulaire()
    return render_template('ajax_table_cours.html', title='Page Cours', form=f, form_filtre=ff)

@app.route('/delete_cours/',methods=['POST'])
@login_required
def delete_cours():
    delete_un_cours_bd(request.form["id"])
    return ""

@app.route("/save/cours/", methods =("POST",))
@login_required
def save_cours():
    f = CoursForm()
    f.remplir_formulaire()
    if f.validate_on_submit():
        o = Cours(nomc = f.nom_cours.data, descc = f.description_cours.data, typec = f.type_cours.data, duree = f.duree_cours.data, id_m = f.prenom_moniteur.data, jours_semaine=f.jours_semaine.data,heure_debut=f.heure_debut_cours.data)
        db.session.add(o)
        db.session.commit()
        return redirect(url_for("index_cours"))
    ff = CoursFormFiltre()
    ff.remplir_formulaire()
    return render_template("ajax_table_cours.html", form=f, open_modal=True, form_filtre = ff)

@app.route("/edit/cours/", methods =("POST",))
@login_required
def edit_cours():
    f = CoursForm()
    f.remplir_formulaire()
    if f.validate_on_submit():
        cours = get_cours_by_id(f.id_c.data)
        cours.nomc = f.nom_cours.data
        cours.descc = f.description_cours.data
        cours.typec = f.type_cours.data
        cours.duree = f.duree_cours.data
        cours.heure_debut = f.heure_debut_cours.data
        cours.jours_semaine = f.jours_semaine.data
        cours.id_m = f.prenom_moniteur.data
        db.session.add(cours)
        db.session.commit()
        return redirect(url_for("index_cours"))
    ff = CoursFormFiltre()
    ff.remplir_formulaire()
    return render_template("ajax_table_cours.html", form=f, open_modal_edit=True, form_filtre = ff)


@app.route('/update_moniteur_options', methods=['POST'])
@login_required
def update_moniteur_options():
    jours_semaine = request.form['jours_semaine']
    duree = request.form['duree']
    heure_debut = request.form['heure_debut']
    #Conversion des str en times
    heure_debut = datetime.strptime(heure_debut, '%H:%M').time()
    duree = datetime.strptime(duree, '%H:%M').time()
    #Recupere la liste des moniteurs dispo a ces horraires
    print("Mes dates")
    print(jours_semaine)
    print(heure_debut)
    print(duree)
    moniteurs = get_liste_moniteur(jours_semaine, heure_debut, duree)
    if not moniteurs:
        return jsonify([])
    else:
        return jsonify([{'id': moniteur.id, 'nom':moniteur.personne.nom_personne, 'prenom': moniteur.personne.prenom_personne} for moniteur in moniteurs])


#Reservation ------------------------------------------------------------------------


class ReservationForm(FlaskForm):
    def validate_poney(form, field):
        id_cl = form.nom_prenom_client.data
        id_po = field.data
        client = get_client_by_id(id_cl)
        poney = get_poney_by_id(id_po)
        if client is not None and poney is not None:
            if(poney.poidssup < client.personne.poids):
                raise ValidationError("Poids client doit etre inferieur a poids poney, Choisissez un autre Poney")
    def validate_client(form, field):
        id_cl = field.data
        if id_cl != "None":
            client = get_client_by_id(id_cl)
            if not client.cotisation:
                raise ValidationError("Le client n'a pas payé la cotisation annuelle")
        print("l470")
    def donnee_valide_client(form, field):
        id_cl = field.data
        if id_cl == "None":
            raise ValidationError("Veuillez selectionner un client")
    def donnee_valide_cours(form, field):
        id_c = field.data
        if id_c == "None":
            raise ValidationError("Veuillez selectionner un cours")
    def date_valide(form, field):
        cours = get_cours_by_id(form.nom_cours.data)
        if DICO_JOURS_SEMAINE[field.data.weekday()] != cours.jours_semaine:
            raise ValidationError("Ce cours n'est pas disponible ce jours ci")

    def client_dispo(form, field):
        id_cl = form.nom_prenom_client.data
        id_cour = form.nom_cours.data
        date_cour = form.date_reservation.data
        if not is_dispo_client(id_cl, date_cour, id_cour):
            raise ValidationError("Le client n'est pas disponnible pour cet réservation")

    def nombre_place_cours(form, field):
        cours = get_cours_by_id(field.data)
        date_reservation = form.date_reservation.data
        nb_personne_cours = get_nombre_personne_dans_cours(cours.idc, date_reservation)
        if(cours.typec == "Collectif" and nb_personne_cours == 10):
            raise ValidationError("Il n'y a plus de place pour ce cours a cette date")
        elif(cours.typec == "Individuel" and nb_personne_cours == 1):
            raise ValidationError("Il n'y a plus de place pour ce cours a cette date")
  
    nom_prenom_client = SelectField("Nom du client",validators =[DataRequired(), donnee_valide_client, validate_client, client_dispo], default="Veuillez choisir un client")
    nom_cours = SelectField("Nom du cours", validators =[DataRequired(), donnee_valide_cours, nombre_place_cours])
    date_reservation = DateField("Date de la réservation", validators =[DataRequired(message="You need to enter the start date"), date_valide])
    nom_poney = SelectField("Nom du poney", validators =[DataRequired(), validate_poney])
    
    ancien_nom_prenom_client = HiddenField() # champ caché pour stocker l'ancien nom et prénom du client
    ancien_nom_cours = HiddenField() # champ caché pour stocker l'ancien nom du cours
    ancien_date_reservation = HiddenField() # champ caché pour stocker l'ancienne date de réservation
    ancien_nom_poney = HiddenField() # champ caché pour stocker l'ancien nom du poney
    
    def get_form_field(self):
        return ['nom_prenom_client', 'nom_cours', 'date_reservation', 'nom_poney']

    def remplir_formulaire(self):
        liste_cours = [(None,"Veuillez choisir un cours")]
        liste_poney = [(None,"Veuillez choisir un poney")]
        liste_client = [(None,"Veuillez choisir un client")]
        if not (current_user.est_moniteur()):
            liste_client.append((current_user.id,"id " + str(current_user.id) + ": "+ current_user.personne.prenom_personne+" "+current_user.personne.nom_personne))
        else:
            liste_client = [(None,"Veuillez choisir un client")]
            for client in get_client():
                liste_client.append((client.id,"id " + str(client.id) + ": "+ client.personne.prenom_personne+" "+client.personne.nom_personne))
        for cours in get_cours():
            liste_cours.append((cours.idc,  "id " + str(cours.idc) + ": " +cours.nomc + " "+ cours.jours_semaine+ " "+str(cours.heure_debut.hour)+"h"+str(cours.heure_debut.minute)))
        for poney in get_poney():
            liste_poney.append((poney.idpo,  "id " + str(poney.idpo) + ": " +poney.nom_poney))
        
        self.nom_prenom_client.choices = liste_client
        self.nom_cours.choices = liste_cours
        self.nom_poney.choices = liste_poney


class ReservationFormFiltre(FlaskForm):
    nom_prenom_client_filtre = SelectField("Nom du client")
    nom_cours_filtre = SelectField("Nom du cours")
    date_reservation_filtre = DateField("Date de la réservation")
    nom_poney_filtre = SelectField("Nom du poney")

    def get_form_field(self):
        return ['nom_prenom_client_filtre', 'nom_cours_filtre', 'date_reservation_filtre', 'nom_poney_filtre']

    def remplir_formulaire(self):
        liste_client = [(None,"Aucun Filtre")]
        liste_cours = [(None,"Aucun Filtre")]
        liste_poney = [(None,"Aucun Filtre")]
        if not (current_user.est_moniteur()):
            liste_client.append((current_user.id,"id " + str(current_user.id) + ": "+ current_user.personne.prenom_personne+" "+current_user.personne.nom_personne))
        else:
            liste_client = [(None,"Veuillez choisir un client")]
            for client in get_client():
                liste_client.append((client.id,"id " + str(client.id) + ": "+ client.personne.prenom_personne+" "+client.personne.nom_personne))
        for cours in get_cours():
            liste_cours.append((cours.idc,  "id " + str(cours.idc) + ": " +cours.nomc + " "+ cours.jours_semaine+ " "+str(cours.heure_debut.hour)+"h"+str(cours.heure_debut.minute)))
        for poney in get_poney():
            liste_poney.append((poney.idpo,  "id " + str(poney.idpo) + ": " +poney.nom_poney))
        
        self.nom_prenom_client_filtre.choices = liste_client
        self.nom_cours_filtre.choices = liste_cours
        self.nom_poney_filtre.choices = liste_poney


@app.route('/api/data/reservation/', methods = ["GET","POST"])
@login_required
def data_reservation():
    if not (current_user.est_moniteur()):
        id_client = current_user.personne.id
    else:
        id_client = request.form["nom_prenom_client_filtre"]
    id_cours = request.form["nom_cours_filtre"]
    date_reservation = request.form["date_reservation_filtre"]
    id_poney = request.form["nom_poney_filtre"]

    print(id_client)
    query = get_reservation_avec_filtre(id_client, id_cours, date_reservation, id_poney)

    # search filter
    search = request.args.get('search[value]')
    if search:
        query = recherche_reservation(query, search)
    total_filtered = query.count()

    # sorting
    order = []
    i = 0
    while True:
        col_index = request.args.get(f'order[{i}][column]')
        if col_index is None:
            break
        col_name = request.args.get(f'columns[{col_index}][data]')
        descending = request.args.get(f'order[{i}][dir]') == 'desc'
        col = get_attribut_reservation(col_name)
        if descending:
            col = col.desc()
        order.append(col)
        i += 1
    if order:
        query = query.order_by(*order)


    # pagination
    start = request.args.get('start', type=int)
    length = request.args.get('length', type=int)
    query = query.offset(start).limit(length)
    # response
    return {
        'data': [reservation.to_dict() for reservation in query],
        'recordsFiltered': total_filtered,
        'recordsTotal': reservation_count(),
        'draw': request.args.get('draw', type=int),
    }

@app.route('/voirReservation/')
@login_required
def index_reservation():
    f = ReservationForm()
    f.remplir_formulaire()
    ff = ReservationFormFiltre()
    ff.remplir_formulaire()
    return render_template('ajax_table_reservation.html', title='Page Réservation', form=f, form_filtre = ff)

@app.route("/save/reservation/", methods =("POST",))
@login_required
def save_reservation():
    f = ReservationForm()
    f.remplir_formulaire()
    if f.validate_on_submit():
        id_cl = f.nom_prenom_client.data
        id_po = f.nom_poney.data
        o = Reserver(date_cours = f.date_reservation.data, id_cl = id_cl, idc = f.nom_cours.data, idpo = id_po)
        db.session.add(o)
        db.session.commit()
        o = Histo_paiement(date = f.date_reservation.data, idcl= id_cl, prix=20)
        db.session.add(o)
        db.session.commit()
        return redirect(url_for("index_reservation"))
    ff = ReservationFormFiltre()
    ff.remplir_formulaire()
    return render_template("ajax_table_reservation.html", form=f, open_modal=True, form_filtre = ff)


@app.route("/edit/reservation/", methods =("POST",))
@login_required
def edit_reservation():
    f = ReservationForm()
    f.remplir_formulaire()
    if f.validate_on_submit():
        reservation = get_reservation_by_date_cours_client(f.ancien_date_reservation.data, f.ancien_nom_cours.data, f.ancien_nom_prenom_client.data)
        reservation.date_cours = f.date_reservation.data
        reservation.id_cl = f.nom_prenom_client.data
        reservation.idc = f.nom_cours.data
        reservation.idpo = f.nom_poney.data
        db.session.add(reservation)
        db.session.commit()
        return redirect(url_for("index_reservation"))
    ff = ReservationFormFiltre()
    ff.remplir_formulaire()
    return render_template("ajax_table_reservation.html", form=f, open_modal_edit=True, form_filtre = ff)

@app.route('/delete_reservation/',methods=['POST'])
def delete_reservation():
    delete_une_reservation_bd(request.form["date_c"], request.form["id_cl"], request.form["id_c"])
    return ""


# Paiement -------------------------------------------------------------------------------


class PaiementForm(FlaskForm):
    def donnee_valide_client(form, field):
        id_cl = field.data
        if id_cl == "None":
            raise ValidationError("Veuillez selectionner un client")
    nom_prenom_client = SelectField("Nom du client",validators =[DataRequired(), donnee_valide_client], default="Veuillez choisir un client")
    #date_reservation  = datetime.today()
    date_reservation = DateField("Date de la réservation")
    prix = FloatField("Prix")
    
    
    ancien_nom_prenom_client = HiddenField() # champ caché pour stocker l'ancien nom et prénom du client
    ancien_date_reservation = HiddenField() # champ caché pour stocker l'ancienne date de réservation
    
    def get_form_field(self):
        return ['nom_prenom_client', 'date_reservation', 'prix']

    def get_form_field_client(self):
        return ['nom_prenom_client']

    def remplir_formulaire(self):
        liste_client = [(None,"Veuillez choisir un client")]
        if not (current_user.est_moniteur()):
            liste_client.append((current_user.id,"id " + str(current_user.id) + ": "+ current_user.personne.prenom_personne+" "+current_user.personne.nom_personne))
        else:
            liste_client = [(None,"Veuillez choisir un client")]
            for client in get_client():
                liste_client.append((client.id,"id " + str(client.id) + ": "+ client.personne.prenom_personne+" "+client.personne.nom_personne))
        self.nom_prenom_client.choices = liste_client

class PaiementFormFiltre(FlaskForm):
    nom_prenom_client_filtre = SelectField("Nom du client")
    #date_reservation_filtre = datetime.today()
    date_reservation_filtre = DateField("Date de la réservation")
    prix_filtre = FloatField("Prix")

    def get_form_field(self):
        return ['nom_prenom_client_filtre', 'date_reservation_filtre', 'prix_filtre']

    def remplir_formulaire(self):
        liste_client = [(None,"Aucun Filtre")]
        if not (current_user.est_moniteur()):
            liste_client.append((current_user.id,"id " + str(current_user.id) + ": "+ current_user.personne.prenom_personne+" "+current_user.personne.nom_personne))
        else:
            liste_client = [(None,"Veuillez choisir un client")]
            for client in get_client():
                liste_client.append((client.id,"id " + str(client.id) + ": "+ client.personne.prenom_personne+" "+client.personne.nom_personne))
        
        self.nom_prenom_client_filtre.choices = liste_client

@app.route('/api/data/paiement/', methods = ["GET", "POST"])
@login_required
def data_paiement():
    print(request.form)
    if not (current_user.est_moniteur()):
        id_client = current_user.personne.id
    else:
        id_client = request.form["nom_prenom_client_filtre"]
    date_reservation = request.form["date_reservation_filtre"]
    prix = request.form["prix_filtre"]

    query = get_paiement_avec_filtre(id_client, date_reservation, prix)
    # search filter
    search = request.args.get('search[value]')
    if search:
        query = recherche_paiements(query, search)
    total_filtered = query.count()

    # sorting
    order = []
    i = 0
    while True:
        col_index = request.args.get(f'order[{i}][column]')
        if col_index is None:
            break
        col_name = request.args.get(f'columns[{col_index}][data]')
        descending = request.args.get(f'order[{i}][dir]') == 'desc'
        col = get_attribut_paiements(col_name)
        if descending:
            col = col.desc()
        order.append(col)
        i += 1
    if order:
        query = query.order_by(*order)


    # pagination
    start = request.args.get('start', type=int)
    length = request.args.get('length', type=int)
    query = query.offset(start).limit(length)
    # response
    return {
        'data': [paiement.to_dict() for paiement in query],
        'recordsFiltered': total_filtered,
        'recordsTotal': paiements_count(),
        'draw': request.args.get('draw', type=int),
    }


@app.route("/edit/paiement/", methods =("POST",))
@login_required
def edit_paiement():
    f = PaiementForm()
    f.remplir_formulaire()
    if f.validate_on_submit():
        reservation = get_reservation_by_date_cours_client(f.ancien_date_reservation.data, f.ancien_nom_cours.data, f.ancien_nom_prenom_client.data)
        reservation.date_cours = f.date_reservation.data
        reservation.id_cl = f.nom_prenom_client.data
        reservation.idc = f.nom_cours.data
        reservation.idpo = f.nom_poney.data
        db.session.add(reservation)
        db.session.commit()
        return redirect(url_for("index_reservation"))
    ff = PaiementFormFiltre()
    ff.remplir_formulaire()
    return render_template("ajax_table_reservation.html", form=f, open_modal_edit=True, form_filtre = ff)

@app.route('/voirPaiement/')
@login_required
def index_paiement():
    f = PaiementForm()
    f.remplir_formulaire()
    ff = PaiementFormFiltre()
    ff.remplir_formulaire()
    return render_template('ajax_table_paiement.html', title='Page Paiement', form=f, form_filtre = ff)

@app.route('/delete_paiement/',methods=['POST'])
@login_required
def delete_paiement():
    delete_un_paiement_bd(request.form["date_c"], request.form["id_cl"], request.form["id_h"])
    return ""

@app.route("/save/paiement/", methods =("POST",))
@login_required
def save_paiement():
    f = PaiementForm()
    f.remplir_formulaire()
    if f.validate_on_submit():
        id_cl = f.nom_prenom_client.data
        if current_user.est_moniteur():
            o = Histo_paiement(date = f.date_reservation.data, idcl = id_cl, prix = f.prix.data)
        else:
            date_aujourdhui = datetime.today()
            o = Histo_paiement(date = date_aujourdhui, idcl = id_cl, prix = 500)
            client = get_client_by_id(id_cl)
            client.cotisation = True
            db.session.add(client)
            db.session.commit()
        db.session.add(o)
        db.session.commit()
        return redirect(url_for("index_paiement"))
    ff = ReservationFormFiltre()
    ff.remplir_formulaire()
    return render_template("ajax_table_paiement.html", form=f, open_modal=True, form_filtre = ff)


"""@app.route('/update_date_options', methods=['POST'])
def update_date_options():
    cours_id = request.form['cours_id']
    cours = get_cours_by_id(cours_id)
    if not cours:
        return []
    available_dates = []
    today = datetime.now()
    end_date = datetime.strptime("31-08-"+str(today.year), "%d-%m-%Y")
    current_date = today
    print(current_date)
    print(end_date)
    while current_date <= end_date:
        print(current_date.weekday())
        print(cours.jours_semaine)
        if DICO_JOURS_SEMAINE[current_date.weekday()] == cours.jours_semaine:
            available_dates.append(current_date.strftime("%Y-%m-%d"))
        current_date += timedelta(days=1)
    print(available_dates)
    return available_dates"""