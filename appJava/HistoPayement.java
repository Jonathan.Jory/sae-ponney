import java.sql.Date;

public class HistoPayement {
    int idPay;
    Date datePayement;
    double montant;

    public HistoPayement(int idPay, Date datePayement, double montant) {
        this.idPay = idPay;
        this.datePayement = datePayement;
        this.montant = montant;
    }
    
    public int getIdPay() {
        return idPay;
    }
    public void setIdPay(int idPay) {
        this.idPay = idPay;
    }
    public Date getDatePayement() {
        return datePayement;
    }
    public void setDatePayement(Date datePayement) {
        this.datePayement = datePayement;
    }
    public double getMontant() {
        return montant;
    }
    public void setMontant(double montant) {
        this.montant = montant;
    }
    
    
}
