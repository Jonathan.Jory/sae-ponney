-- verifier poid cl < poidPonney
-- OK
delimiter |
create or replace TRIGGER poidValide before insert on RESERVER for each row
begin
    declare mes varchar(100);
    declare poidClient int(9);
    declare poidponey int(9);

    select poidsup into poidponey from PONEY where new.idpo = idpo;
    select poids into poidClient from PERSONNE where idp = new.idp;

    if  (poidClient > poidponey) then
        set mes = concat("Le poids du client : ", poidClient, "kg est supérieur à celui supporté par le poney : ", poidponey, "kg");
    signal SQLSTATE '45000' set MESSAGE_TEXT = mes;
    end if;
end |
delimiter ;

insert into RESERVER(heureD, dateD, idc, idpo, idp) values (9,'2022-09-28', 1, 2, 4);

-- verifier cotisation
-- OK
delimiter |
create or replace TRIGGER cotisationOk before insert on RESERVER for each row
begin
    declare mes varchar(100);
    declare paye boolean;

    select cotisation into paye from CLIENT where new.idp = idp;

    if  (paye = "False") then
        set mes = concat("Le client n'a pas payé la cotisation");
    signal SQLSTATE '45000' set MESSAGE_TEXT = mes;
    end if;
end |
delimiter ;

insert into RESERVER(heureD, dateD, idc, idpo, idp) values (10,'2022-09-28', 1, 4, 13);


-- verification si une personne pas encore client
-- OK
delimiter |
create or replace TRIGGER estClient before insert on RESERVER for each row
begin
    declare mes varchar(100);
    declare clientOk int(9);

    select ifnull(count(*),0) into clientOk from CLIENT where new.idp = idp;

    if (clientOk = 0) then
        set mes = concat("La personne n'est pas cliente");
    signal SQLSTATE '45000' set MESSAGE_TEXT = mes;
    end if;
end |
delimiter ;

insert into RESERVER(heureD, dateD, idc, idpo, idp) values (10,'2022-09-28', 1, 4, 45);

-- verification si une personne est pas moniteur lors de la création d'un cour
-- OK
delimiter |
create or replace TRIGGER estMoniteur before insert on COURS for each row
begin
    declare mes varchar(100);
    declare moniteurOk int(9);

    select ifnull(count(*),0) into moniteurOk from MONITEUR where new.idp = idp;

    if (moniteurOk = 0) then
        set mes = concat("La personne n'est pas un Moniteur");
    signal SQLSTATE '45000' set MESSAGE_TEXT = mes;
    end if;
end |
delimiter ;

insert into COURS(idc, idp, nomc, typec, descriptionc, duree) values (1,6,"test", "test","test",'02:00');

-- vérifier pas de chevauchement d'heure pour un poney plus temps de repos respecter

delimiter |
create or replace TRIGGER heureValidePoney before insert on RESERVER for each row
begin
    declare mes varchar(300);
    declare heureDeb Time;
    declare idcour int(9);
    declare idNewcour int(9);
    declare dureeC Time;
    declare dureeCNew Time;
    declare attente Time default 0;
    declare fini boolean DEFAULT false;
    declare attenteNew Time default 0;
    declare lesReservation cursor for 
        select heureD, idc from RESERVER where new.idpo = idpo and dateD = new.dateD;
    declare continue handler for not found set fini = true;
    open lesReservation;
    while not fini do
        fetch lesReservation into heureDeb, idcour;
        if not fini then
            select duree into dureeC from COURS where idc = idcour;
            select duree into dureeCNew from COURS where idc = new.idc;
            if (dureeC >= 2) then
                set attente = '1:00:00';
            end if;
            if (dureeCNew >= 2) then
                set attenteNew = '1:00:00';
            end if;
            if (new.heureD = heureDeb) then
                set mes = concat("Le poney n'est pas disponnible à: ", new.heureD, " Il est soit en repos soit déjà en cour");
                signal SQLSTATE '45000' set MESSAGE_TEXT = mes;
            end if;
            if (new.heureD > heureDeb and new.heureD < heureDeb+dureeC+attente) then
                set mes = concat("Le poney n'est pas disponnible à: ", new.heureD, " Il est soit en repos soit déjà en cour");
                signal SQLSTATE '45000' set MESSAGE_TEXT = mes;
            end if;
            if (new.heureD < heureDeb and new.heureD+dureeCNew+attenteNew > heureDeb) then
                set mes = concat("Le poney n'est pas disponnible à: ", new.heureD, " Il est soit en repos soit déjà en cour");
                signal SQLSTATE '45000' set MESSAGE_TEXT = mes;
            end if;
        end if;
    end while;
    close lesReservation;
end |
delimiter ;

insert into RESERVER(heureD, dateD, idc, idpo, idp) values ('7:00','2022-10-03', 1, 5, 11);



-- vérifier pas de chevauchement d'heure pour un client
-- OK

delimiter |
create or replace TRIGGER heureValideClient before insert on RESERVER for each row
begin
    declare mes varchar(300);
    declare heureDeb Time;
    declare idcour int(9);
    declare idNewcour int(9);
    declare dureeC Time;
    declare dureeCNew Time;
    declare fini boolean DEFAULT false;
    declare lesReservation cursor for 
        select heureD, idc from RESERVER where new.idp = idp and dateD = new.dateD;
    declare continue handler for not found set fini = true;
    open lesReservation;
    while not fini do
        fetch lesReservation into heureDeb, idcour;
        if not fini then
            select duree into dureeC from COURS where idc = idcour;
            select duree into dureeCNew from COURS where idc = new.idc;
            if (new.heureD = heureDeb) then
                set mes = concat("Le Client n'est pas disponnible à: ", new.heureD, " Il est déjà en cour");
                signal SQLSTATE '45000' set MESSAGE_TEXT = mes;
            end if;
            if (new.heureD > heureDeb and new.heureD < heureDeb+dureeC) then
                set mes = concat("Le Client n'est pas disponnible à: ", new.heureD, " Il est déjà en cour");
                signal SQLSTATE '45000' set MESSAGE_TEXT = mes;
            end if;
            if (new.heureD < heureDeb and new.heureD + dureeCNew > heureDeb) then
                set mes = concat("Le Client n'est pas disponnible à: ", new.heureD, " Il est déjà en cour");
                signal SQLSTATE '45000' set MESSAGE_TEXT = mes;
            end if;
        end if;
    end while;
    close lesReservation;
end |
delimiter ;

insert into RESERVER(heureD, dateD, idc, idpo, idp) values ('7:30','2022-10-03', 1, 4, 20);


-- toutes les vérifications pas finis
delimiter |
create or replace TRIGGER avantReservation before insert on RESERVER for each row
begin
    declare mes varchar(100);
    declare poidClient int(9);
    declare poidponey int(9);
    declare paye boolean;

    select poidsup into poidponey from PONEY where new.idpo = idpo;
    select poids into poidClient from PERSONNE where idp = new.idcl;

    select cotisation into paye from CLIENT where new.idcl = idcl;

    if  (paye = "False") then
        set mes = concat("Le client n'a pas payé la cotisation");
    signal SQLSTATE '45000' set MESSAGE_TEXT = mes;
    end if;
    if  (poidClient > poidponey) then
        set mes = concat("Le poids du client : ", poidClient, "kg est supérieur à celui supporté par le poney : ", poidponey, "kg");
    signal SQLSTATE '45000' set MESSAGE_TEXT = mes;
    end if;
end |
delimiter ;

-- verif heure max cour

-- ajout personne horraire


