import java.sql.ResultSet;
import java.sql.*;
import java.util.List;
import java.util.ArrayList;

public class PoneyBD {
    private ConnexionMySQL connexion;

    public PoneyBD(ConnexionMySQL connexion){
        this.connexion = connexion;
    }

    /**
     * Il renvoie l'identifiant maximum de la table PERSONNE
     * 
     * @return L'identifiant maximal de la personne dans la base de données.
     */
    public int maxIdPersonne(){
        int id = -1;
        try{
            Statement s = this.connexion.getConnexion().createStatement();
            ResultSet rs = s.executeQuery("select max(idp)id from PERSONNE");
            rs.next();
            id = rs.getInt("id");
            
        }

        catch(SQLException e){
            System.out.println("Erreur :"+e);
        }
        return id;
    }

    /**
     * Il renvoie l'identifiant le plus élevé d'un poney dans la base de données
     * 
     * @return L'identifiant maximum de la table PONEY
     */
    public int maxIdPoney(){
        int id = -1;
        try{
            Statement s = this.connexion.getConnexion().createStatement();
            ResultSet rs = s.executeQuery("select max(idpo)id from PONEY");
            rs.next();
            id = rs.getInt("id");
            
        }

        catch(SQLException e){
            System.out.println("Erreur :"+e);
        }
        return id;
    }

    /**
     * Il renvoie l'identifiant maximum des cours dans la base de données
     * 
     * @return L'identifiant maximum du cours
     */
    public int maxIdCours(){
        int id = -1;
        try{
            Statement s = this.connexion.getConnexion().createStatement();
            ResultSet rs = s.executeQuery("select max(idc)id from COURS");
            rs.next();
            id = rs.getInt("id");
            
        }

        catch(SQLException e){
            System.out.println("Erreur :"+e);
        }
        return id;
    }

    /**
     * Il renvoie l'identifiant maximum de la table HISTOPAYEMENT.
     * 
     * @return L'identifiant maximum de la table HISTOPAYEMENT
     */
    public int maxIdHistoPayement(){
        int id = -1;
        try{
            Statement s = this.connexion.getConnexion().createStatement();
            ResultSet rs = s.executeQuery("select max(idPay)id from HISTOPAYEMENT");
            rs.next();
            id = rs.getInt("id");
            
        }

        catch(SQLException e){
            System.out.println("Erreur :"+e);
        }
        return id;
    }


    /**
     * Il ajoute un poney à la base de données
     * 
     * @param nomPo le nom du poney
     * @param poidSup le poids supporté du poney
     * @param racePo la race du poney
     * @param robe la couleur du poney
     */
    public void ajouterPoney(String nomPo, double poidSup, String racePo, String robe){
        try{
            PreparedStatement ps = this.connexion.getConnexion().prepareStatement("insert into PONEY values (?,?,?,?,?)");
            ps.setInt(1, maxIdPoney()+1);
            ps.setString(2, nomPo);
            ps.setDouble(3, poidSup);
            ps.setString(4, racePo);
            ps.setString(5, robe);
            ps.executeUpdate();
            System.out.println("L'insertion s'est bien passé");
            
        }     
        catch(SQLException err){
            System.out.println("Il y a eu une erreur lors de l'insertion");
            System.out.println("Erreur :"+err);
        }
    }

    /**
     * Il ajoute un nouveau moniteur à la base de données
     * 
     * @param nomP le nom de la personne
     * @param prenomP prénom
     * @param age son age
     * @param poids son poids
     * @param numTel Son numéro de tel
     */
    public void ajouterMoniteur(String nomP, String prenomP, int age, double poids, String numTel){
        try{
            PreparedStatement ps = this.connexion.getConnexion().prepareStatement("insert into PERSONNE values (?,?,?,?,?,?)");
            ps.setInt(1, maxIdPersonne()+1);
            ps.setString(2, nomP);
            ps.setString(3, prenomP);
            ps.setInt(4, age);
            ps.setDouble(5, poids);
            ps.setString(6, numTel);
            ps.executeUpdate();
            PreparedStatement ps2 = this.connexion.getConnexion().prepareStatement("insert into MONITEUR values (?)");
            ps2.setInt(1, maxIdPersonne());
            ps2.executeUpdate();
            System.out.println("L'insertion s'est bien passé");
            
        }     
        catch(SQLException err){
            System.out.println("Il y a eu une erreur lors de l'insertion");
            System.out.println("Erreur :"+err);
        }
    }


    /**
     * Il ajoute un client à la base de données
     * 
     * @param nomP le nom du client
     * @param prenomP prénom
     * @param age son age
     * @param poids son poids
     * @param numTel Son numéro de tel
     */
    public void ajouterClient(String nomP, String prenomP, int age, double poids, String numTel){
        try{
            PreparedStatement ps = this.connexion.getConnexion().prepareStatement("insert into PERSONNE values (?,?,?,?,?,?)");
            ps.setInt(1, maxIdPersonne()+1);
            ps.setString(2, nomP);
            ps.setString(3, prenomP);
            ps.setInt(4, age);
            ps.setDouble(5, poids);
            ps.setString(6, numTel);
            ps.executeUpdate();
            PreparedStatement ps2 = this.connexion.getConnexion().prepareStatement("insert into CLIENT values (?,?)");
            ps2.setInt(1, maxIdPersonne());
            ps2.setBoolean(2, false);
            ps2.executeUpdate();
            System.out.println("L'insertion s'est bien passé");
            
        }     
        catch(SQLException err){
            System.out.println("Il y a eu une erreur lors de l'insertion");
            System.out.println("Erreur :"+err);
        }
    }

    /**
     * Il ajoute un cours à la base de données
     * 
     * @param idp l'identifiant du moniteur du cours
     * @param nomC nom du cours
     * @param typeC Type du cours individuel ou collectif
     * @param descriptionC descriptif du cours
     * @param duree Temps du cours
     */
    // 
    public void ajouterCours(int idp, String nomC, String typeC, String descriptionC, Time duree){
        try{
            PreparedStatement ps = this.connexion.getConnexion().prepareStatement("insert into COURS values (?,?,?,?,?,?)");
            ps.setInt(1, maxIdCours()+1);
            ps.setInt(2, idp);
            ps.setString(3, nomC);
            ps.setString(4, typeC);
            ps.setString(5, descriptionC);
            ps.setTime(6, duree);
            ps.executeUpdate();
            System.out.println("L'insertion s'est bien passé");
            
        }     
        catch(SQLException err){
            System.out.println("Il y a eu une erreur lors de l'insertion");
            System.out.println("Erreur :"+err);
        }
    }

    /**
     * Il ajoute une réservation à la base de données.
     * 
     * @param heureD heure de debut
     * @param dateD Date de la réservation
     * @param idC identifiant du cours
     * @param idPo identifiant du poney
     * @param idp identifiant du client
     */
    public void ajouterReservation(Time heureD, Date dateD, int idC, int idPo, int idp){
        try{
            PreparedStatement ps = this.connexion.getConnexion().prepareStatement("insert into RESERVER values (?,?,?,?,?)");
            ps.setTime(1, heureD);
            ps.setDate(2, dateD);
            ps.setInt(3, idC);
            ps.setInt(4, idPo);
            ps.setInt(5, idp);
            ps.executeUpdate();
            System.out.println("L'insertion s'est bien passé");
            
        }     
        catch(SQLException err){
            System.out.println("Il y a eu une erreur lors de l'insertion");
            System.out.println("Erreur :"+err);
        }
    }


    /**
     * Il ajoute un paiement à la base de données poiur une personne
     * 
     * @param date la date du paiement
     * @param montant la somme d'argent versée
     * @param idp l'identifiant du patient
     */
    public void ajouterPayement(Date date, double montant, int idp){
        try{
            PreparedStatement ps = this.connexion.getConnexion().prepareStatement("insert into HISTOPAYEMENT values (?,?,?)");
            ps.setInt(1, maxIdHistoPayement()+1);
            ps.setDate(2, date);
            ps.setDouble(3, montant);
            ps.executeUpdate();
            PreparedStatement ps2 = this.connexion.getConnexion().prepareStatement("insert into REALISE values (?,?)");
            ps2.setInt(1, maxIdHistoPayement());
            ps2.setInt(2, idp);
            ps2.executeUpdate();
            PreparedStatement ps3 = this.connexion.getConnexion().prepareStatement("update CLIENT set cotisation = ? where idp = ?");
            ps3.setBoolean(1, true);
            ps3.setInt(2, idp);
            ps3.executeUpdate();
            System.out.println("L'insertion s'est bien passé");

            
        }     
        catch(SQLException err){
            System.out.println("Il y a eu une erreur lors de l'insertion");
            System.out.println("Erreur :"+err);
        }
    }
    

    
    /**
     * Il supprime un client de la base de données
     * 
     * @param id l'identifiant du client
     */
    public void supprimerClient(int id){
        try{
            supprimerRealiseClient(id);
            PreparedStatement ps0 = this.connexion.getConnexion().prepareStatement("select * from RESERVER where idp = ?");
            ps0.setInt(1, id);
            ResultSet rs = ps0.executeQuery();
            while (rs.next()){
                supprimerReservation(rs.getTime(1), rs.getDate(2), rs.getInt(3), rs.getInt(5));
            }
            PreparedStatement ps = this.connexion.getConnexion().prepareStatement("delete from CLIENT where idp = ?");
            PreparedStatement ps2 = this.connexion.getConnexion().prepareStatement("delete from PERSONNE where idp = ?");
            ps.setInt(1, id);
            ps2.setInt(1, id);
            int result = ps.executeUpdate();
            int result2 = ps2.executeUpdate();
            if (result != 0 && result2 != 0){
                System.out.println("Le Client a bien été supprimer");
            }
            else{
                System.out.println("Le Client n'existe pas");
            }
        }     
        catch(SQLException err){
            System.out.println("Il y a eu une erreur lors de la suppression ");
            System.out.println("Erreur :"+err);
        }
    }


    /**
     * Il supprime un enseignant de la base de données
     * 
     * @param id l'identifiant du formateur à supprimer
     */
    public void supprimerMoniteur(int id){
        try{
            PreparedStatement ps0 = this.connexion.getConnexion().prepareStatement("select * from COURS where idp = ?");
            ps0.setInt(1, id);
            ResultSet rs = ps0.executeQuery();
            while (rs.next()){
                supprimerCours(rs.getInt(1));
            }
            PreparedStatement ps = this.connexion.getConnexion().prepareStatement("delete from MONITEUR where idp = ?");
            PreparedStatement ps2 = this.connexion.getConnexion().prepareStatement("delete from PERSONNE where idp = ?");
            ps.setInt(1, id);
            ps2.setInt(1, id);
            int result = ps.executeUpdate();
            int result2 = ps2.executeUpdate();
            if (result != 0 && result2 != 0){
                System.out.println("Le Moniteur a bien été supprimer");
            }
            else{
                System.out.println("Le Moniteur n'existe pas");
            }
            
        }     
        catch(SQLException err){
            System.out.println("Il y a eu une erreur lors de la suppression ");
            System.out.println("Erreur :"+err);
        }
    }


    /**
     * Il supprime un cours de la base de données
     * 
     * @param id l'identifiant du cours à supprimer
     */
    public void supprimerCours(int id){
        try{
            PreparedStatement ps0 = this.connexion.getConnexion().prepareStatement("select * from RESERVER where idc = ?");
            ps0.setInt(1, id);
            ResultSet rs = ps0.executeQuery();
            while (rs.next()){
                supprimerReservation(rs.getTime(1), rs.getDate(2), id, rs.getInt(5));
            }
            PreparedStatement ps = this.connexion.getConnexion().prepareStatement("delete from COURS where idc = ?");
            ps.setInt(1, id);
            int result = ps.executeUpdate();
            if (result != 0){
                System.out.println("Le cours a bien été supprimer");
            }
            else{
                System.out.println("Le cours n'existe pas");
            }
            
        }     
        catch(SQLException err){
            System.out.println("Il y a eu une erreur lors de la suppression ");
            System.out.println("Erreur :"+err);
        }
    }

    /**
     * Il supprime une réservation de la base de données
     * 
     * @param heureD Temps
     * @param dateD Date de la réservation
     * @param idC identifiant du tribunal
     * @param idCl identifiant du client
     */
    public void supprimerReservation(Time heureD, Date dateD, int idC, int idCl){
        try{
            PreparedStatement ps = this.connexion.getConnexion().prepareStatement("delete from RESERVER where heureD = ? and dateD = ? and idc = ? and idp = ?");
            ps.setTime(1, heureD);
            ps.setDate(2, dateD);
            ps.setInt(3, idC);
            ps.setInt(4, idCl);
            int result = ps.executeUpdate();
            if (result != 0){
                System.out.println("La Réservation a bien été supprimer");
            }
            else{
                System.out.println("La Reservation n'existe pas");
            }
            
        }     
        catch(SQLException err){
            System.out.println("Il y a eu une erreur lors de la suppression ");
            System.out.println("Erreur :"+err);
        }
    }


    /**
     * Il supprime un poney de la base de données
     * 
     * @param id l'id du poney à supprimer
     */
    public void supprimerPoney(int id){
        try{
            PreparedStatement ps0 = this.connexion.getConnexion().prepareStatement("select * from RESERVER where idpo = ?");
            ps0.setInt(1, id);
            ResultSet rs = ps0.executeQuery();
            while (rs.next()){
                supprimerReservation(rs.getTime(1), rs.getDate(2), rs.getInt(3), rs.getInt(5));
            }
            PreparedStatement ps = this.connexion.getConnexion().prepareStatement("delete from PONEY where idpo = ?");
            ps.setInt(1, id);
            int result = ps.executeUpdate();
            if (result != 0){
                System.out.println("Le Poney a bien été supprimer");
            }
            else{
                System.out.println("Le Poney n'existe pas");
            }
            
        }     
        catch(SQLException err){
            System.out.println("Il y a eu une erreur lors de la suppression ");
            System.out.println("Erreur :"+err);
        }
    }

    /**
     * Il supprime toutes les lignes de la table REALIZE qui ont le même idp que celui passé en
     * paramètre
     * 
     * @param idp l'identifiant du client
     */
    public void supprimerRealiseClient(int idp){
        try{
            PreparedStatement ps0 = this.connexion.getConnexion().prepareStatement("select * from REALISE where idp = ?");
            ps0.setInt(1, idp);
            ResultSet rs0 = ps0.executeQuery();
            while (rs0.next()){
                PreparedStatement ps1 = this.connexion.getConnexion().prepareStatement("DELETE from REALISE where idp = ? and idPay = ?");
                ps1.setInt(1, idp);
                ps1.setInt(2, rs0.getInt(1));
                ps1.executeUpdate();
            }            
        }     
        catch(SQLException err){
            System.out.println("Il y a eu une erreur lors de la suppression ");
            System.out.println("Erreur :"+err);
        }
    }


    /**
     * Il affiche tous les poneys de la base de données
     */
    public void afficherTousLesPoney(){
        boolean trouve = false;
        try{
            Statement s = this.connexion.getConnexion().createStatement();
            ResultSet rs = s.executeQuery("select * from PONEY");
            while (rs.next()){
                trouve = true;
                Poney poney = new Poney(rs.getInt(1), rs.getString(2), rs.getDouble(3), rs.getString(4), rs.getString(4));
                System.out.println(poney);
            }
        }     
        catch(SQLException err){
            System.out.println("Erreur :"+err);
        }
        if (!trouve){
            System.out.println("Il n'y a pas de poney");
        }
    }
    /**
     * Il affiche tous les clients de la base de données
     */
    public void afficherTousLesCliens(){
        boolean trouve = false;
        try{
            Statement s = this.connexion.getConnexion().createStatement();
            ResultSet rs = s.executeQuery("select * from CLIENT");
            while (rs.next()){
                PreparedStatement ps0 = this.connexion.getConnexion().prepareStatement("select * from PERSONNE where idp = ?");
                ps0.setInt(1, rs.getInt(1));
                ResultSet rs0 = ps0.executeQuery();
                rs0.next();
                trouve = true;
                Client client = new Client(rs.getInt(1), rs0.getString(2), rs0.getString(3),rs0.getInt(4), rs0.getDouble(5), rs0.getString(6), rs.getBoolean(2));
                System.out.println(client);
            }
        }     
        catch(SQLException err){
            System.out.println("Erreur :"+err);
        }
        if (!trouve){
            System.out.println("Il n'y a pas de client");
        }
    }
    /**
     * Il affiche tous les moniteurs de la base de données
     */
    public void afficherTousLesMoniteurs(){
        boolean trouve = false;
        try{
            Statement s = this.connexion.getConnexion().createStatement();
            ResultSet rs = s.executeQuery("select * from MONITEUR");
            while (rs.next()){
                PreparedStatement ps0 = this.connexion.getConnexion().prepareStatement("select * from PERSONNE where idp = ?");
                ps0.setInt(1, rs.getInt(1));
                ResultSet rs0 = ps0.executeQuery();
                rs0.next();
                trouve = true;
                Moniteur moniteur = new Moniteur(rs0.getInt(1), rs0.getString(2), rs0.getString(3),rs0.getInt(4), rs0.getDouble(5), rs0.getString(6));//   
                System.out.println(moniteur);
            }
        }     
        catch(SQLException err){
            System.out.println("Erreur :"+err);
        }
        if (!trouve){
            System.out.println("Il n'y a pas de moniteur");
        }
    }
    /**
     * Il affiche tous les cours de la base de données
     */
    public void afficherTousLesCours(){
        boolean trouve = false;
        try{
            Statement s = this.connexion.getConnexion().createStatement();
            ResultSet rs = s.executeQuery("select * from COURS");
            while (rs.next()){
                trouve = true;
                Cours cours = new Cours(rs.getInt(1), rs.getInt(2), rs.getString(3),rs.getString(4), rs.getString(5), rs.getTime(6)); 
                System.out.println(cours);
            }
        }     
        catch(SQLException err){
            System.out.println("Erreur :"+err);
        }
        if (!trouve){
            System.out.println("Il n'y a pas de cours");
        }
    }
    /**
     * Il affiche toutes les réservations dans la base de données
     */
    public void afficherToutesLesReservations(){
        boolean trouve = false;
        try{
            Statement s = this.connexion.getConnexion().createStatement();
            ResultSet rs = s.executeQuery("select * from RESERVER");
            while (rs.next()){
                trouve = true;
                Reserver reserver = new Reserver(rs.getTime(1), rs.getDate(2), rs.getInt(3), rs.getInt(4), rs.getInt(5)); 
                System.out.println(reserver);
            }
        }     
        catch(SQLException err){
            System.out.println("Erreur :"+err);
        }
        if (!trouve){
            System.out.println("Il n'y a pas de reservation");
        }
    }
    /**
     * Il affiche un poney par son identifiant
     * 
     * @param id l'id du poney que vous souhaitez afficher
     */
    public void afficherPoneyById(int id){
        boolean trouve = false;
        try{
            Statement s = this.connexion.getConnexion().createStatement();
            ResultSet rs = s.executeQuery("select * from PONEY where idpo = "+ id);
            while (rs.next()){
                trouve = true;
                Poney poney = new Poney(rs.getInt(1), rs.getString(2), rs.getDouble(3), rs.getString(4), rs.getString(4));
                System.out.println(poney);
            }
        }     
        catch(SQLException err){
            System.out.println("Erreur :"+err);
        }
        if (!trouve){
            System.out.println("Il n'y a pas de poney avec l'id: "+ id);
        }
    }
    /**
     * Il affiche le client avec l'identifiant donné
     * 
     * @param id l'identifiant du client
     */
    public void afficherClientById(int id){
        boolean trouve = false;
        try{
            Statement s = this.connexion.getConnexion().createStatement();
            ResultSet rs = s.executeQuery("select * from CLIENT where idp = "+ id);
            while (rs.next()){
                trouve = true;
                Client client = new Client(rs.getInt(1), rs.getString(2), rs.getString(3),rs.getInt(4), rs.getDouble(5), rs.getString(6), rs.getBoolean(7));
                System.out.println(client);
            }
        }     
        catch(SQLException err){
            System.out.println("Erreur :"+err);
        }
        if (!trouve){
            System.out.println("Il n'y a pas de client avec l'id: "+id);
        }
    }
    /**
     * Cette fonction permet d'afficher un moniteur par son identifiant
     * 
     * @param id l'identifiant du moniteur
     */
    public void afficherMoniteurById(int id){
        boolean trouve = false;
        try{
            Statement s = this.connexion.getConnexion().createStatement();
            ResultSet rs = s.executeQuery("select * from MONITEUR where idp = "+ id);
            while (rs.next()){
                trouve = true;
                Moniteur moniteur = new Moniteur(rs.getInt(1), rs.getString(2), rs.getString(3),rs.getInt(4), rs.getDouble(5), rs.getString(6));//   
                System.out.println(moniteur);
            }
        }     
        catch(SQLException err){
            System.out.println("Erreur :"+err);
        }
        if (!trouve){
            System.out.println("Il n'y a pas de moniteur avec l'id: "+id);
        }
    }
    /**
     * Cette fonction permet d'afficher un cours par son identifiant
     * 
     * @param id l'identifiant du cours
     */
    public void afficherCoursById(int id){
        boolean trouve = false;
        try{
            Statement s = this.connexion.getConnexion().createStatement();
            ResultSet rs = s.executeQuery("select * from COURS where idp = "+ id);
            while (rs.next()){
                trouve = true;
                Cours cours = new Cours(rs.getInt(1), rs.getInt(2), rs.getString(3),rs.getString(4), rs.getString(5), rs.getTime(6)); 
                System.out.println(cours);
            }
        }     
        catch(SQLException err){
            System.out.println("Erreur :"+err);
        }
        if (!trouve){
            System.out.println("Il n'y a pas de cours avec l'id: "+id);
        }
    }
    /**
     * Cette fonction permet d'afficher tous les cours à une date données
     * 
     * @param date la date
     */
    public void afficherReservationsByDate(Date date){
        boolean trouve = false;
        try{
            PreparedStatement ps = this.connexion.getConnexion().prepareStatement("select * from RESERVER where dateD = ?");
            ps.setDate(1, date);
            ResultSet rs = ps.executeQuery();
            while (rs.next()){
                trouve = true;
                Reserver reserver = new Reserver(rs.getTime(1), rs.getDate(2), rs.getInt(3), rs.getInt(4), rs.getInt(5)); 
                System.out.println(reserver);
               
            }
        }     
        catch(SQLException err){
            System.out.println("Erreur :"+err);
        }
        if (!trouve){
            System.out.println("Il n'y a pas de reservation à la date donnée");
        }
    }
}




    

