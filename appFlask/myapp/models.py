from sqlalchemy import Integer, BOOLEAN,Column, ForeignKey, String, func
from sqlalchemy.orm import declarative_base,relationship
from flask_login import UserMixin
from .app import db, login_manager

import random
import string
from datetime import timedelta, date, time, datetime

def generate_password(length=8):
    """
    Il renvoie une chaîne de caractères aléatoires de longueur "longueur" à partir de l'ensemble de
    toutes les lettres, chiffres et signes de ponctuation ASCII

    :param length: La longueur du mot de passe à générer. La valeur par défaut est 8, defaults to 8
    (optional)
    :return: Une chaîne de caractères aléatoires
    """
    #liste toutes les lettres minuscules, majuscules et chiffres ASCII
    characters = string.ascii_letters + string.digits + string.punctuation
    #fonction random.sample pour obtenir une liste d’éléments aléatoires dans la liste de caractères
    password = ''.join(random.sample(characters, length))
    return password


class Personne(db.Model, UserMixin):
    # Il crée une table dans la base de données avec le nom personne.
    tablename = 'personne'
    id = db.Column(db.Integer, primary_key = True)
    nom_personne = db.Column(db.String)
    prenom_personne = db.Column(db.String)
    poids = db.Column(db.Numeric(4,1))
    num_tel = db.Column(db.String)
    age_personne = db.Column(db.Integer)
    password = db.Column(db.String(64))


    def __init__(self, nom_personne, prenom_personne, poids, num_tel, 
        age_personne, id=None, password=None):
        """
        initialise la classe personne

        :param nom_personne: Le nom de la personne
        :param prenom_personne: Prénom
        :param poids: masse
        :param num_tel: numéro de téléphone
        :param age_personne: âge de la personne
        :param id: l'identifiant de l'utilisateur
        :param password: le mot de passe de l'utilisateur
        """
        if id is None:
            self.id = id
        if password is None:
            self.password = generate_password()
        else:
            self.password = password
        self.nom_personne = nom_personne
        self.prenom_personne = prenom_personne
        self.poids = poids
        self.num_tel = num_tel
        self.age_personne = age_personne


    def to_dict(self):
        """
        Il prend un objet et renvoie une représentation du dictionnaire de cet objet
        :return: Un dictionnaire avec les clés et les valeurs de l'objet.
        """
        return {
            'id': self.id,
            'nom_personne': self.nom_personne,
            'prenom_personne': self.prenom_personne,
            'poids': self.poids,
            'num_tel': self.num_tel,
            'age_personne': self.age_personne
        }

    def __repr__(self) -> str:
        """
        La fonction __repr__ est utilisée pour renvoyer une représentation sous forme de chaîne de
        l'objet
        :return: L'identifiant, le nom, le prénom, l'âge et le numéro de téléphone de la personne.
        """
        return str(self.id) + ", " + self.nom_personne + ", " + self.prenom_personne+ ", " + str(self.age_personne)+ ", " + self.num_tel

    def get_id(self):
        """
        Il renvoie l'identifiant de l'objet
        :return: L'identifiant de l'objet.
        """
        return self.id
    def get_id_personne(self):
        """
        Il renvoie l'id_personne de la personne.
        :return: L'attribut id_personne de l'objet.
        """
        return self.id_personne

    def set_id_personne(self, id_personne:int):
        """
        Il définit l'attribut id_personne de l'objet sur la valeur du paramètre id_personne.

        :param id_personne: entier
        :type id_personne: int
        """
        self.id_personne = id_personne

    def get_prenom_personne(self):
        """
        Il renvoie le prenom_personne de la personne.
        :return: Le prenom_personne
        """
        return self.prenom_personne

    def set_prenom_personne(self, prenom_personne:str):
        """
        Cette fonction définit l'attribut prenom_personne de l'objet Personne à la valeur du paramètre
        prenom_personne

        :param prenom_personne: Le prénom de la personne
        :type prenom_personne: str
        """
        self.prenom_personne = prenom_personne

    def get_nom_personne(self):
        """
        Il renvoie le nom de la personne.
        :return: Le nom de la personne.
        """
        return self.nom_personne

    def set_nom_personne(self, nom_personne:str):
        """
        La fonction set_nom_personne prend un objet Personne et une chaîne comme arguments et définit
        l'attribut nom_personne de l'objet Personne sur la chaîne

        :param nom_personne: Le nom de la personne
        :type nom_personne: str
        """
        self.nom_personne = nom_personne

    def get_age(self):
        """
        La fonction get_age() renvoie l'âge de la personne
        :return: L'âge de la personne
        """
        return self.age

    def set_age(self, age:int):
        """
        Cette fonction prend un entier et définit l'âge de l'objet sur cet entier

        :param age: L'âge de la personne
        :type age: int
        """
        self.age = age

    def get_num_tel(self):
        """
        Il retourne la valeur de l'attribut num_tel de l'objet self
        :return: Le numéro du téléphone.
        """
        return self.num_tel

    def set_numt_tel(self, tel:str):
        """
        La fonction set_numt_tel() prend une chaîne en argument et l'affecte à l'attribut num_tel

        :param tel: Le numéro de téléphone de la personne
        :type tel: str
        """
        self.num_tel = tel

    def get_poids(self):
        """
        Il renvoie le poids de la personne.
        :return: Le poids de la personne.
        """
        return self.poids

    def set_poids(self, poids:float):
        """
        Cette fonction fixe le poids de la personne à la valeur du paramètre poids

        :param poids: Le poids de la personne
        :type poids: float
        """
        self.poids = poids

    def get_max_id(self, session):
        """
        Il renvoie l'identifiant max de la table, ou 0 si la table est vide

        :param session: l'objet de session
        :return: Le max_id est renvoyé.
        """
        max_id = session.query(func.max(self.id))
        if max_id[0] is None:
            max_id = 0
            return max_id
        return max_id[0]




@login_manager.user_loader
def load_user(personne_id):
    """
    Si l'utilisateur est un moniteur, retournez l'objet moniteur, sinon retournez l'objet client.
    </code>
    :param personne_id: L'user_id que Flask-Login transmet à la fonction de rappel
    :return: L'objet utilisateur.
    """
    # since the user_id is just the primary key of our user table, use it in the query for the user
    if est_moniteur(personne_id):
        return get_moniteur_by_id(personne_id)
    else:
        return get_client_by_id(personne_id)


def get_personne_email_mdp(name, mdp):
    """
    Il renvoie le premier objet personne qui correspond au nom et au mot de passe
    :param name: le nom de la personne
    :param mdp: le mot de passe
    :return: La personne
    """
    return Personne.query.filter(Personne.prenom_personne == name).filter(Personne.password == mdp).first()

# La classe Moniteur hérite de la classe db.Model et de la classe UserMixin.
class Moniteur(db.Model, UserMixin):
    tablename = 'moniteur'
    id = db.Column(db.Integer,db.ForeignKey("personne.id"), primary_key = True)
    personne    = db.relationship("Personne",backref =db.backref("personnes_", lazy="dynamic"))
    

    def to_dict(self):
        """
        Il prend l'objet self, qui est une instance de la classe Personne, et renvoie un dictionnaire
        des attributs de l'objet
        :return: Un dictionnaire
        """
        dico = self.personne.to_dict()
        return dico

    def get_id(self):
        """
        Il renvoie l'identifiant de l'objet
        :return: L'identifiant de l'objet.
        """
        return self.id
    def get__id_moniteur(self):
        """
        Il renvoie l'id_personne du moniteur.
        :return: L'attribut id_personne de l'objet.
        """
        return self.id_personne

    def set_id_moniteur(self,  id_personne:int):
        """
        La fonction set_id_moniteur prend en paramètre self et id_personne, et fixe l'attribut
        id_personne de l'objet self à la valeur du paramètre id_personne

        :param id_personne: entier
        :type id_personne: int
        """
        self.id_personne = id_personne

    def __repr__(self) -> str:
        """
        Cette fonction renvoie une chaîne qui représente l'objet moniteur
        :return: L'identifiant du moniteur
        """
        return "Le moniteur : "+str(self.id)
    def est_moniteur(self):
        """
        Il renvoie Vrai.
        :return: Vrai
        """
        return True
    def getattr_personne(self):
        return self.personne.nom_personne



# La classe Client hérite de la classe db.Model et de la classe UserMixin.
# La classe Client a une relation un-à-un avec la classe Personne, a un attribut contribution, personne, id.
class Client(db.Model, UserMixin):
    __tablename__ = 'client'
    #id= db.Column(db.Integer,db.ForeignKey("personne.id"),primary_key = True)
    id = db.Column(db.Integer,db.ForeignKey("personne.id"),primary_key = True)
    cotisation =  db.Column(db.BOOLEAN)
    #personne    = db.relationship("Personne",backref=db.backref("personnes", lazy="dynamic"))
    personne    = db.relationship("Personne",backref=db.backref("personnes", lazy="dynamic"))

    def to_dict(self):
        """
        Il prend un objet de type client et retourne un dictionnaire avec les mêmes informations
        :return: Un dictionnaire avec le nom et la cotisation de la personne.
        """
        dico = self.personne.to_dict()
        dico["cotisation"] = self.cotisation
        return dico

    def get__id_client(self):
        """
        Il renvoie l'id_personne du client.
        :return: L'attribut id_personne de l'client.
        """
        return self.id_personne

    def set_id_client(self,  id_personne:int):
        """
        La fonction set_id_client() prend un paramètre  id_personne, qui est un entier, et fixe la
        valeur de l'attribut id_personne de l'objet à la valeur du paramètre id_personne

        :param id_personne: entier
        :type id_personne: int
        """
        self.id_personne = id_personne

    def get_id(self):
        """
        Il renvoie l'identifiant de l'objet
        :return: L'identifiant de l'objet.
        """
        return self.id

    def is_cotisation(self):
        """
        Il renvoie la valeur de l'attribut cotisation de l'objet.
        :return: La valeur du champ cotisation.
        """
        return self.cotisation

    def set_cotisation(self, cotisation: bool):
        """
        Cette fonction définit l'attribut cotisation de l'objet sur la valeur du paramètre cotisation

        :param cotisation: si le client à payé
        :type cotisation: bool
        """
        self.cotisation = cotisation


    def __repr__(self) -> str:
        """
        La fonction __repr__() est une fonction spéciale dans les classes Python. Elle est similaire à
        la méthode toString() en Java. Le but de la fonction __repr__() est de calculer la réputation de
        chaîne "officielle" d'un objet. En d'autres termes, cette fonction calcule la chaîne qui serait
        imprimée par la fonction print()
        :return: Le nom de la personne et si elle a payé ou non
        """
        res = str(self.personne)

        if self.cotisation:
            res += " il a payé"
        else:
            res += " il n'a payé"
        return res
    def est_moniteur(self):
        """
        Il renvoie Faux.
        :return: Faux
        """
        return False


# Il crée une table dans la base de données.
class Poney(db.Model):
    __tablename__ = 'poney'
    idpo = db.Column(db.Integer, primary_key = True)
    nom_poney = db.Column(db.String)
    poidssup = db.Column(db.Numeric(4,1))
    race = db.Column(db.String)
    robe = db.Column(db.String)
    image = db.Column(db.String)

    def to_dict(self):
        """
        Il prend un objet Poney et renvoie un dictionnaire avec les mêmes données
        :return: Un dictionnaire avec les clés et les valeurs de l'objet.
        """
        return {
            'idpo': self.idpo,
            'nom_poney': self.nom_poney,
            'poidssup': self.poidssup,
            'race': self.race,
            'robe': self.robe,
            'image': self.image
        }

    def __repr__(self) -> str:
        """
        Cette fonction renvoie une chaîne contenant l'identifiant, le nom, la race, la couleur et le
        poids maximum supporter du poney
        :return: L'identifiant, le nom, la race, la couleur et le poids maximum du poney.
        """
        return f"id {self.idpo}, prénom: {self.nom_poney}, race: {self.race}, robe: {self.robe}, poids max supporté: {self.poidssup}kg"

    def get_id_poney(self):
        """
        Il renvoie l'identifiant du poney.
        :return: L'id_poney
        """
        return self.id_poney

    def set_id_poney(self, id_poney:int):
        """
        Cette fonction définit l'attribut id_poney de l'objet à la valeur du paramètre id_poney

        :param id_poney: L'identifiant du poney
        :type id_poney: int
        """
        self.id_poney = id_poney

    def get_nom_poney(self):
        """
        Il renvoie le nom du poney.
        :return: Le nom du poney
        """
        return self.nom_poney

    def set_nom_poney(self, nom_poney:str):
        """
        Cette fonction définit le nom du poney

        :param nom_poney: Le nom du poney
        :type nom_poney: str
        """
        self.nom_poney = nom_poney

    def get_race(self):
        """
        La fonction get_race() renvoie la race du poney
        :return: La race du poney.
        """
        return self.race

    def set_race(self, race:str):
        """
        Cette fonction prend une chaîne et définit l'attribut race de l'objet sur cette chaîne

        :param race: La race du poney
        :type race: str
        """
        self.race = race

    def get_robe(self):
        """
        La fonction get_robe() renvoie la valeur de l'attribut robe de l'objet self
        :return: L'attribut robe du poney.
        """
        return self.robe

    def set_robe(self, robe:str):
        """
        La fonction set_robe() prend une chaîne en argument et l'affecte à l'attribut robe

        :param robe: La robe du poney(sa couleur)
        :type robe: str
        """
        self.robe = robe

    def get_poids_max(self):
        """
        :return: Le poids maximal supporté.
        """
        return self.poids_max

    def set_poids_max(self, poids_max:float):
        """
        Cette fonction définit le poids maximum supporté

        :param poids_max: Le poids maximum
        :type poids_max: float
        """
        self.poids_max = poids_max

    def get_image_poney(self):
        """
        Il renvoie l'image du poney
        :return: L'image du poney
        """
        return self.image

    def set_image_poney(self, image:String):
        """
        Cette fonction définit l'image du poney

        :param image: L'image du poney
        :type image: String
        """
        self.image = image

class Histo_paiement(db.Model):
    __tablename__='histo_paiement'
    idh = db.Column(db.Integer, primary_key = True)
    idcl = db.Column(db.Integer, ForeignKey("client.id"))
    date = db.Column(db.DATE)
    prix = db.Column(db.Numeric(5,2))
    client = relationship("Client")

    def __init__(self, date, prix, idcl, idh=-1):
        """
        La fonction __init__() est un constructeur qui est appelé lorsqu'un objet d'une classe est
        instancié
        
        :param date: date de paiement
        :param prix: le prix
        :param idcl: identité du client
        :param idh: l'identifiant du paiement
        """

        if idh != -1:
            self.idh = idh
        self.date = date
        self.prix = prix
        self.idcl = idcl


    def __repr__(self) -> str:
        """
        La fonction __repr__() est utilisée pour renvoyer une représentation sous forme de chaîne de
        l'objet
        :return: L'identifiant du paiement, l'identifiant du client, le prix et la date.
        """
        return str(self.idh) + ", L'id du client: " + str(self.idcl) + ", " + str(self.prix) + "€, le :" + str(self.date)

    def to_dict(self):
        """
        Il convertit l'objet en dictionnaire
        :return: Un dictionnaire
        """
        return {
            'idh': self.idh,
            'idcl': self.idcl,
            'prenom_nom_client': self.client.personne.prenom_personne+" "+self.client.personne.nom_personne,
            'prix': self.prix,
            'date': str(self.date),
        }

# Il crée une classe appelée Cours, qui est une table dans la base de données.
class Cours(db.Model):
    __tablename__ = 'cours'
    idc = db.Column(db.Integer, primary_key = True)
    nomc = db.Column(db.String)
    descc = db.Column(db.String)
    typec = db.Column(db.String)
    duree = db.Column(db.Time)
    heure_debut = db.Column(db.TIME)
    jours_semaine = db.Column(db.String)
    id_m = db.Column(db.Integer, ForeignKey("moniteur.id"))
    moniteur = relationship("Moniteur")
    # prix = db.Column(db.DECIMAL)


    def __init__(self, nomc, descc, typec, duree, heure_debut, jours_semaine, id_m, idc=-1):
        """
        Un constructeur pour la classe Cours.

        :param nomc: nom du cours
        :param descc: descriptif du cours
        :param typec: "Collectif" or "Individuel"
        :param duree: durée du cours en minutes
        :param heure_debut: temps
        :param jours_semaine: une liste des jours de la semaine (lundi, mardi, etc.)
        :param id_m: identifiant de l'entraîneur
        :param idc: l'identifiant du cours
        """
        if idc != -1:
            self.idc = idc
        self.nomc = nomc
        self.descc = descc
        self.typec = typec
        self.duree = duree
        self.heure_debut = heure_debut
        self.jours_semaine = jours_semaine
        self.id_m = id_m
        if typec == "Collectif":
            self.nb_place = 10
        else:
            self.nb_place = 1


    def __repr__(self) -> str:
        """
        La fonction renvoie une chaîne qui contient les éléments qui composent un cours
        :return: L'identifiant du cours, le nom du cours, la description du cours, le type de cours, la
        durée du cours et l'identifiant du moniteur.
        """
        return str(self.idc) + ", " + self.nomc + ", " + self.descc + ", " + self.typec + " dure : " + str(self.duree) + ", Moniteur d'id : "+ str(self.id_m) # " coute : " + str(self.prix)

    def to_dict(self):
        """
        Il convertit l'objet en dictionnaire
        :return: Un dictionnaire
        """
        return {
            'idc': self.idc,
            'nomc': self.nomc,
            'descc': self.descc,
            'typec': self.typec,
            'duree': str(self.duree.hour)+"h"+str(self.duree.minute)+"min",
            'heure_debut': str(self.heure_debut.hour)+"h"+str(self.heure_debut.minute),
            'jours_semaine': self.jours_semaine,
            'prenom_nom_moniteur': self.moniteur.personne.prenom_personne+" "+self.moniteur.personne.nom_personne,
            'id_moniteur': self.id_m,
        }

# Reserver est une classe qui a un date_cours, id_cl, idc, idpo, poney, client et cours
class Reserver(db.Model):
    __tablename__ = 'reserver'
    date_cours = db.Column(db.DATE, primary_key = True)
    id_cl = db.Column(db.Integer, ForeignKey("client.id"), primary_key=True)
    idc = db.Column(db.Integer,ForeignKey("cours.idc"), primary_key=True)
    idpo = db.Column(db.Integer,ForeignKey("poney.idpo"))
    poney = db.relationship("Poney")
    client = db.relationship("Client",backref =db.backref("clients__", lazy="dynamic"))
    cours = db.relationship("Cours")


    def __repr__(self) -> str:
        """
        La fonction __repr__() est utilisée pour calculer la représentation sous forme de chaîne
        "officielle" d'un objet.
        """
        return  "Le " + str(self.date_cours) + ", le client " + str(self.client.personne.prenom_personne) + ", avec le poney " + str(self.poney.nom_poney)# + ", " + str(self.a_paye)


    def to_dict(self):
        """
        Il prend un objet Cours_Client et retourne un dictionnaire avec les attributs de l'objet
        :return: un dictionaire avec les clés suivantes:
        id_cl
        idc
        idpo
        nom_poney
        prenom_nom_client
        date_cours
        nom_cours
        prenom_nom_moniteur
        """
        return {
            'id_cl': self.id_cl,
            'idc': self.idc,
            'idpo': self.idpo,
            'nom_poney': self.poney.nom_poney,
            'prenom_nom_client': self.client.personne.prenom_personne+" "+self.client.personne.nom_personne,
            'date_cours': str(self.date_cours),
            'nom_cours': self.cours.nomc,
            'prenom_nom_moniteur': self.cours.moniteur.personne.prenom_personne+" "+self.cours.moniteur.personne.nom_personne,
        }



#Poney
def get_sample_poney():
    """
    Il rapporte 10 poneys
    :return: Une liste d'objets Poney.
    """
    return Poney.query.limit(10).all()

def delete_un_poney_bd(id):
    """
    Il supprime un poney de la base de données
    :param id: l'id du poney à supprimer
    """
    Poney.query.filter(Poney.idpo == id).delete()
    db.session.commit()

def get_poney():
    """
    Il renvoie les poneys
    :return: Les poneys
    """
    return Poney.query

def get_poney_avec_filtre(nom_poney, poidssup, race, robe):
    """
    Il renvoie une liste de poneys qui correspondent aux filtres donnés
    
    :param nom_poney: Le nom du poney
    :param poidssup: Le poids supporter par le poney
    :param race: La course du poney
    :param robe: La couleur du poney
    :return: Une liste de poneys
    """
    poneys = get_poney()
    if(nom_poney != ""):
        poneys = poneys.filter(Poney.nom_poney == nom_poney)
    if(poidssup != ""):
        poneys = poneys.filter(Poney.poidssup == poidssup)
    if(race != ""):
        poneys = poneys.filter(Poney.race == race)
    if(robe != ""):
        poneys = poneys.filter(Poney.robe == robe)
    return poneys


def recherche_poney(query, search):
    """
    Il prend une requête et une chaîne de recherche, et renvoie une requête qui filtre la requête
    d'origine par la chaîne de recherche

    :param query: l'objet de requête que vous souhaitez filtrer
    :param search: le terme de recherche
    :return: Un objet filtré
    """
    return query.filter(db.or_(
            Poney.idpo.like(f'%{search}%'),
            Poney.nom_poney.like(f'%{search}%')
        ))

def get_attribut_poney(col_name):
    """
    Il retourne la valeur de l'attribut de la classe Poney dont le nom est passé en paramètre

    :param col_name: le nom de la colonne dont vous voulez obtenir l'attribut
    :return: La valeur de l'attribut de l'objet Poney.
    """
    return getattr(Poney, col_name)

def poney_count():
    """
    Il renvoie le nombre de poneys dans la base de données
    :return: Le nombre de poneys dans la base de données.
    """
    return Poney.query.count()

def get_poney_by_id(id):
    """
    Il renvoie un poney de la base de données, étant donné son identifiant

    :param id: L'identifiant du poney que vous souhaitez obtenir
    :return: Un objet poney
    """
    return Poney.query.get(id)
#Paiement
def delete_un_paiement_bd(id):
    """
    Il supprime un paiements de la base de données

    :param id: l'id du paiements à supprimer
    """
    Histo_paiement.query.filter(Histo_paiement.idh == id).delete()
    db.session.commit()

def get_paiement():
    """
    Ils renvoie les paiements
    :return: Les paiements
    """
    return Histo_paiement.query

def get_paiement_avec_filtre(idcl, date, prix):
    """
    Il renvoie une liste de tous les paiements dans la base de données, filtrés par les paramètres
    passés à la fonction
    
    :param idh: l'identifiant de l'hôtel
    :param idcl: identité du client
    :param date: La date du paiement
    :param prix: Le prix du produit
    :return: Une liste de paiements
    """
    paiements = get_paiement()
    if(idcl != "None"):
        paiements = paiements.filter(Histo_paiement.idcl == idcl)
    if(date != ""):
        paiements = paiements.filter(Histo_paiement.date == date)
    if(prix != ""):
        paiements = paiements.filter(Histo_paiement.prix == prix)
    return paiements


def recherche_paiements(query, search):
    """
    Il prend une requête et une chaîne de recherche, et renvoie une requête qui filtre la requête
    d'origine par la chaîne de recherche

    :param query: l'objet requête
    :param search: le terme de recherche
    :return: resultat de la recherche
    """
    return query.filter(db.or_(
            Histo_paiement.id.like(f'%{search}%')
        ))

def get_attribut_paiements(col_name):
    """
    Elle retourne la valeur de l'attribut de la classe des paiements dont le nom est passé en paramètre

    :param col_name: le nom de la colonne dans la base de données
    :return: La valeur de l'attribut de l'objet des paiements.
    """
    return getattr(Histo_paiement, col_name)

def paiements_count():
    """
    Elle renvoie le nombre de lignes de la table des paiements
    :return: Le nombre de lignes dans la table des paiements.
    """
    return Histo_paiement.query.count()

def get_paiement_by_id(id):
    """
    Il renvoie un objet paiements de la base de données, ou une erreur 404 si le paiements n'existe pas

    :param id: L'identifiant du paiements à récupérer
    :return: Le paiements
    """
    return Histo_paiement.query.get_or_404(id)

#Moniteur
def delete_un_moniteur_bd(id):
    """
    Il supprime un moniteur de la base de données

    :param id: l'id du moniteur à supprimer
    """
    cours = Cours.query.filter(Cours.id_m == id).all()
    for cour in cours:
        Reserver.query.filter(Reserver.idc == cour.idc).delete()
    Moniteur.query.filter(Moniteur.id == id).delete()
    Personne.query.filter(Personne.id == id).delete()
    db.session.commit()

def get_moniteur():
    """
    Ils renvoie les moniteurs
    :return: Les moniteurs
    """
    return Moniteur.query.join(Personne, Personne.id == Moniteur.id)

def get_moniteur_avec_filtre(nom, prenom, age, poids, num_tel):
    """
    Il renvoie une liste de tous les moniteurs de la base de données, filtrée par les paramètres
    
    :param nom: le nom de la personne
    :param prenom: Prénom
    :param age: âge de la personne
    :param poids: masse
    :param num_tel: Le numéro de téléphone de la personne
    :return: A list of moniteurs
    """
    moniteurs = get_moniteur()
    if(nom != ""):
        moniteurs = moniteurs.filter(Personne.nom_personne == nom)
    if(prenom != ""):
        moniteurs = moniteurs.filter(Personne.prenom_personne == prenom)
    if(age != ""):
        moniteurs = moniteurs.filter(Personne.age_personne == age)
    if(poids != ""):
        moniteurs = moniteurs.filter(Personne.poids == poids)
    if(num_tel != ""):
        moniteurs = moniteurs.filter(Personne.num_tel == num_tel)
    return moniteurs

def get_moniteur_by_name(nom, prenom):
    """
    Il renvoie le premier moniteur (instructeur) qui a le prénom et le prénom

    :param nom: nom du moniteur
    :param prenom: prenom du moniteur
    :return: Le Moniteur
    """
    moniteur = Moniteur.query.join(Personne).filter(Personne.nom_personne == nom).filter(Personne.prenom_personne == prenom).first()
    return moniteur


def recherche_moniteur(query, search):
    """
    Il prend une requête et une chaîne de recherche, et renvoie une requête qui filtre la requête
    d'origine par la chaîne de recherche

    :param query: l'objet requête
    :param search: le terme de recherche
    :return: resultat de la recherche
    """
    return query.filter(db.or_(
            Moniteur.id.like(f'%{search}%'),
            Moniteur.personne.nom_personne.like(f'%{search}%')
        ))

def get_attribut_moniteur(col_name):
    """
    Elle retourne la valeur de l'attribut de la classe Moniteur dont le nom est passé en paramètre

    :param col_name: le nom de la colonne dans la base de données
    :return: La valeur de l'attribut de l'objet Moniteur.
    """
    return getattr(Moniteur, col_name)

def moniteur_count():
    """
    Elle renvoie le nombre de lignes de la table Moniteur
    :return: Le nombre de lignes dans la table Moniteur.
    """
    return Moniteur.query.count()

def get_moniteur_by_id(id):
    """
    Il renvoie un objet moniteur de la base de données, ou une erreur 404 si le moniteur n'existe pas

    :param id: L'identifiant du moniteur à récupérer
    :return: Le moniteur
    """
    return Moniteur.query.get_or_404(id)

def est_moniteur(id):
    """
    Vérifie si la personne est un moniteur grâce à l'id

    :param id: l'identifiant de la personne
    :return: Une valeur booléenne.
    """
    moniteur = Moniteur.query.get(id)
    if moniteur:
        return True
    else:
        return False


#Client
def delete_un_client_bd(id):
    """
    Il supprime un client de la base de données
    :param id: l'identifiant du client à supprimer
    """
    Reserver.query.filter(Reserver.id_cl == id).delete()
    Histo_paiement.query.filter(Histo_paiement.idcl == id).delete()
    Client.query.filter(Client.id == id).delete()
    Personne.query.filter(Personne.id == id).delete()
    db.session.commit()

def get_client():
    """
    Il renvoie tout les clients
    :return: les clients
    """
    return Client.query.join(Personne, Personne.id == Client.id)


def get_client_avec_filtre(nom, prenom, age, poids, num_tel, cotisation):
    """
    Il renvoie une liste de clients qui correspondent aux critères donnés
    
    :param nom: le nom du client
    :param prenom: Le prénom du client
    :param age: âge du client
    :param poids: masse
    :param num_tel: le numéro de téléphone du client
    :param cotisation: oui, non, ""
    :return: Une liste de clients
    """

    clients = get_client()
    if(nom != ""):
        clients = clients.filter(Personne.nom_personne == nom)
    if(prenom != ""):
        clients = clients.filter(Personne.prenom_personne == prenom)
    if(age != ""):
        clients = clients.filter(Personne.age_personne == age)
    if(poids != ""):
        clients = clients.filter(Personne.poids == poids)
    if(num_tel != ""):
        clients = clients.filter(Personne.num_tel == num_tel)
    if(cotisation != ""):
        if cotisation =="oui":
            clients = clients.filter(Client.cotisation == True)
        elif cotisation =="non":
            clients = clients.filter(Client.cotisation == False)
    return clients

def recherche_client(query, search):
    """
    Il prend une requête et une chaîne de recherche, et renvoie une requête qui filtre la requête
    d'origine par la chaîne de recherche

    :param query: l'objet requête
    :param search: le terme de recherche
    :return: Un objet requête
    """
    return query.filter(db.or_(
            Client.id.like(f'%{search}%'),
            Client.personne.nom_personne.like(f'%{search}%'),
            Client.personne.prenom_personne.like(f'%{search}%')
        ))

def get_attribut_client(col_name):
    """
    Il renvoie la valeur de l'attribut d'un objet, étant donné le nom de cet attribut sous forme de
    chaîne

    :param col_name: le nom de la colonne dans le tableau
    :return: Le nom de la colonne de la table Client.
    """
    return getattr(Client, col_name)

def client_count():
    """
    Il renvoie le nombre de lignes dans la table Client
    :return: Le nombre de clients dans la base de données.
    """
    return Client.query.count()

def get_client_by_id(id):
    """
    Il renvoie un objet client de la base de données, étant donné l'identifiant du client

    :param id: L'identifiant du client à obtenir
    :return: Un objet Client
    """
    return Client.query.get(id)

#Cours
def delete_un_cours_bd(id):
    """
    Il supprime toutes les lignes de la table Reserver qui ont le même idc que l'id passé à la fonction,
    puis il supprime la ligne de la table Cours qui a le même idc que l'id passé à la fonction, puis il
    valide les modifications à la base de données

    :param id: l'identifiant du cours à supprimer
    """
    Reserver.query.filter(Reserver.idc == id).delete()
    Cours.query.filter(Cours.idc == id).delete()
    db.session.commit()

def get_cours():
    """
    Il renvoie tous les cours de la base de données
    :return: les cours
    """
    return Cours.query.join(Moniteur, Cours.id_m == Moniteur.id).join(Personne, Moniteur.id == Personne.id)

def get_cours_avec_filtre(nom_cours, type_cours, jours_semaine, heure_debut, duree_cours, id_moniteur):
    """
    Il renvoie une liste de cours qui correspondent aux filtres donnés
    
    :param nom_cours: Le nom du cours
    :param type_cours: The type of course (e.g. "Cours de groupe")
    :param jours_semaine: Aucun Filtre, Lundi, Mardi, Mercredi, Jeudi, Vendredi, Samedi, Dimanche
    :param heure_debut: L'heure à laquelle le cours commence
    :param duree_cours: Durée du cours
    :param id_moniteur: L'identifiant de l'instructeur
    :return: Une liste de tous les cours qui correspondent au filtre
    """
    cours = get_cours()
    if(nom_cours != ""):
        cours = cours.filter(Cours.nomc == nom_cours)
    if(type_cours != "Aucun Filtre"):
        cours = cours.filter(Cours.typec == type_cours)
    if(jours_semaine != "Aucun Filtre"):
        cours = cours.filter(Cours.jours_semaine == jours_semaine)
    if(heure_debut != ""):
        heure_debut = datetime.strptime(heure_debut, '%H:%M').time()
        cours = cours.filter(Cours.heure_debut == heure_debut)
    if(duree_cours != ""):
        duree_cours = datetime.strptime(duree_cours, '%H:%M').time()
        cours = cours.filter(Cours.duree == duree_cours)
    if(id_moniteur != "None"):
        cours = cours.filter(Moniteur.id == id_moniteur)
    return cours

def recherche_cours(query, search):
    """
    Il joint la table Cours avec la table Moniteur et la table Personne, puis filtre les résultats en
    fonction de la requête de recherche

    :param query: la requête que vous souhaitez filtrer
    :param search: le terme de recherche
    :return: Un objet requête
    """
    return query.join(Moniteur, Moniteur.id == Cours.id_m).join(Personne).filter(db.or_(
            Cours.idc.like(f'%{search}%'),
            Personne.nom_personne.like(f'%{search}%'),
            Personne.prenom_personne.like(f'%{search}%'),
            Cours.jours_semaine.like(f'%{search}%'),
            Cours.typec.like(f'%{search}%'),
            Cours.nomc.like(f'%{search}%')
        ))

def get_attribut_cours(col_name):
    """
    Elle renvoie la valeur de l'attribut de la classe Cours dont le nom est passé en paramètre

    :param col_name: le nom de la colonne dont vous voulez obtenir la valeur
    """
    if col_name == "prenom_nom_moniteur":
        col_name = "moniteur.personne.nom_personne"
    return getattr(Cours, col_name)

def cours_count():
    """
    Il renvoie le nombre de cours dans la base de données
    :return: Le nombre de cours dans la base de données.
    """
    return Cours.query.count()

def get_cours_by_id(id):
    """
    Il renvoie un objet Cours avec l'identifiant donné

    :param id: L'identifiant du cours que vous souhaitez obtenir
    :return: Le Cour rechercher
    """
    return Cours.query.get(id)
#Reservation
def delete_une_reservation_bd(date_cours, id_client, id_cours):
    """
    Il supprime une réservation de la base de données

    :param date_cours: la date du cours
    :param id_client: l'identifiant du client
    :param id_cours: l'identifiant du cours
    """
    Reserver.query.filter(Reserver.date_cours == date_cours).filter(Reserver.id_cl == id_client).filter(Reserver.idc == id_cours).delete()
    db.session.commit()

def get_reservation():
    """
    Renvoie toutes les lignes de Reserver
    """
    return Reserver.query.join(Poney, Reserver.idpo == Poney.idpo).join(Client, Reserver.id_cl == Client.id).join(Cours, Reserver.idc == Cours.idc)


def get_reservation_avec_filtre(id_client, id_cours, date_reservation, id_poney):
    """
    Il renvoie un objet de requête qui est le résultat du filtrage du résultat de la fonction
    get_reservation()
    
    :param id_client: l'identifiant du client
    :param id_cours: l'identifiant du cours
    :param date_reservation: la date de la réservation
    :param id_poney: l'id du poney
    :return: Une liste de réservations
    """
    reservation = get_reservation()

    if(id_client != "None"):
        reservation = reservation.filter(Client.id == id_client)
    if(id_cours != "None"):
        reservation = reservation.filter(Cours.idc == id_cours)
    if(id_poney != "None"):
        reservation = reservation.filter(Poney.idpo == id_poney)
    if(date_reservation != ""):
        reservation = reservation.filter(Reserver.date_cours == date_reservation)
    return reservation

def recherche_reservation(query, search):
    """
    Il joint la table Reserver avec les tables Poney et Cours, puis filtre les résultats en fonction du
    terme de recherche

    :param query: la requête que vous souhaitez filtrer
    :param search: le terme de recherche
    :return: Un objet requête
    """
    return query.join(Poney, Poney.idpo == Reserver.idpo).join(Cours, Cours.idc == Reserver.idc).filter(db.or_(
            Reserver.date_cours.like(f'%{search}%'),
            Reserver.client.personne.prenom_personne.like(f'%{search}%')
        ))

def get_attribut_reservation(col_name):
    """
    Il renvoie la valeur de l'attribut d'une classe étant donné le nom de l'attribut sous forme de
    chaîne

    :param col_name: Le nom de la colonne dont vous voulez obtenir la valeur
    :return: La valeur de l'attribut de l'objet.
    """
    if col_name == 'prenom_nom_client':
        return get_attribut_client("nom_personne")
    return getattr(Reserver, col_name)

def reservation_count():
    """
    Il renvoie le nombre de lignes dans la table Reserver
    :return: Le nombre de réservations dans la base de données.
    """
    return Reserver.query.count()

def get_reservation_by_date_cours_client(date, id_cours, id_client):
    return Reserver.query.filter(Reserver.date_cours == date).filter(Reserver.idc == id_cours).filter(Reserver.id_cl== id_client).first()

def is_valid_reservation(id_poney, reservation_datetime, reservation_duree_time):
    """
    Il vérifie si un poney est disponible pour une date et une heure donnée

    :param id_poney: l'id du poney
    :param reservation_datetime: la date du cour
    :param reservation_duree_time: la duree du cour
    :return: Une liste de listes de tuples.
    """
    liste_cours = []
    # Récupérer toutes les réservations pour ce poney
    reservations = Reserver.query.join(Cours).filter(Reserver.idpo == id_poney).order_by(Cours.heure_debut.asc()).filter(Reserver.date_cours == reservation_datetime.date()).all()
    if len(reservations) == 0:
        return True

    for reservation in reservations:
        liste_cours.append([reservation.cours.heure_debut,time(reservation.cours.heure_debut.hour + reservation.cours.duree.hour,  reservation.cours.heure_debut.minute + reservation.cours.duree.minute ,  reservation.cours.heure_debut.second + reservation.cours.duree.second)])
    liste_cours2 = [liste_cours[0].copy()]
    last_heure_fin = None
    for (heure_debut, heure_fin) in liste_cours[1:]:
        (_, last_heure_fin) = liste_cours2[-1]
        if heure_debut == last_heure_fin:
            liste_cours2[-1][1] = heure_fin
        else:
            liste_cours2.append([heure_debut, heure_fin])
    for(heure_debut, heure_fin) in liste_cours2:
        if reservation_datetime.time() >= heure_debut and reservation_datetime.time() < heure_fin or reservation_datetime.time() <= heure_debut and (reservation_datetime + timedelta(hours=reservation_duree_time.hour, minutes=reservation_duree_time.minute)).time()>=heure_fin:
            #print("Pas dispo")   #Le poney est deja pris
            return False
        if heure_fin > (reservation_datetime - timedelta(hours=1)).time() and heure_fin <= reservation_datetime.time():
            duree_cours_deja_existant = timedelta(hours=heure_fin.hour, minutes= heure_fin.minute, seconds=heure_fin.second) - timedelta(hours=heure_debut.hour, minutes= heure_debut.minute, seconds=heure_debut.second)
            duree_mon_cours = timedelta(hours= reservation_duree_time.hour, minutes=reservation_duree_time.minute, seconds=reservation_duree_time.second)
            if  duree_cours_deja_existant + duree_mon_cours >= timedelta(hours=2):
                #print("trop heure affilé") #le poney n'a pas fait de pause ou pas assez longue
                return False
    return True


def get_list_valid_poney_reservation(client, reservation_datetime, reservation_duree_time):
    """
    Il renvoie une liste de poneys disponibles pour une date et une heure données, et qui conviennent à
    un client donné

    :param client: l'objet client
    :param reservation_datetime: dateheure.dateheure(2020, 5, 5, 10, 0)
    :param reservation_duree_time: la durée de la réservation
    :return: Une liste de poneys
    """
    liste_poney = []
    poneys = get_poney()
    for poney in poneys:
        if is_valid_reservation(poney.idpo, reservation_datetime, reservation_duree_time) and poney.poidssup >= client.personne.poids:
            liste_poney.append(poney)
    return liste_poney


#is_valid_reservation(4, datetime(2023, 6, 12, 19, 00 ,0), time(2, 0, 0))

def get_nombre_personne_dans_cours(id_cours, date_reservation):
    """
    Il renvoie le nombre de clients ayant réservé un cours avec un identifiant donné à une date donnée

    :param id_cours: l'identifiant du cours
    :param date_reservation: 2020-01-01
    :return: Le nombre de clients dans une classe donnée.
    """
    clients = Reserver.query.join(Cours).filter(Cours.idc == id_cours).filter(Reserver.date_cours == date_reservation).all()
    return len(clients)


def is_dispo_moniteur(id_moniteur, jours, heure, duree):
    """
    Si le moniteur n'est pas déjà réservé pour le jour et l'heure donnés, alors il est disponible
    
    :param id_moniteur: l'identifiant du moniteur
    :param jours: le jour de la semaine (0-6)
    :param heure: l'heure du jour (0-23)
    :param duree: Durée du cours
    :return: Une valeur booléenne
    """
    datetime_heure_debut = datetime(1970, 1, 1, hour=heure.hour, minute=heure.minute)
    datetime_heure_fin = datetime(1970, 1, 1, hour=heure.hour, minute=heure.minute) + timedelta(hours = duree.hour, minutes = duree.minute)

    cours = Cours.query.filter(Cours.id_m == id_moniteur).filter(Cours.jours_semaine == jours).all()
    if len(cours) == 0:
        #Si il a aucun cours ce jours la, il est forcement disponible
        return True
    for cour in cours:
        datetime_cours_heure_debut =  datetime(1970, 1, 1, hour=cour.heure_debut.hour, minute=cour.heure_debut.minute)
        datetime_cours_heure_fin =  datetime(1970, 1, 1, hour=cour.heure_debut.hour, minute=cour.heure_debut.minute) + timedelta(hours=cour.duree.hour, minutes=cour.duree.minute)
        #On verifie si les dates se chevauchent
        if not (datetime_cours_heure_fin < datetime_heure_debut or  datetime_heure_fin  < datetime_cours_heure_debut):
            #Elles se chevauchent
            return False
    return True


def get_liste_moniteur(jours, heure, duree):
    moniteurs = Moniteur.query.join(Personne, Moniteur.id == Personne.id)
    liste_moniteur_dispo = []
    for moniteur in moniteurs:
        if is_dispo_moniteur(moniteur.id, jours, heure, duree):
            liste_moniteur_dispo.append(moniteur)
    return liste_moniteur_dispo


def is_dispo_client(id_client, reservation_datetime, id_cour):
    """
    Si le client n'a pas de réservation à la date de la nouvelle réservation, renvoyez Vrai. Si le
    client a des réservations à la date de la nouvelle réservation, vérifiez si la nouvelle réservation
    chevauche l'une des réservations existantes. Si c'est le cas, renvoyez False. Si ce n'est pas le
    cas, retourne True
    
    :param id_client: l'identifiant du client
    :param reservation_datetime: dateheure.dateheure(2020, 5, 5, 0, 0)
    :param id_cour: l'identifiant du cours
    :return: Une valeur booléenne
    """
    new_cour = get_cours_by_id(id_cour)
    datetime_heure_debut = datetime(1970, 1, 1, hour=new_cour.heure_debut.hour, minute=new_cour.heure_debut.minute)
    datetime_heure_fin = datetime(1970, 1, 1, hour=new_cour.heure_debut.hour, minute=new_cour.heure_debut.minute) + timedelta(hours = new_cour.duree.hour, minutes = new_cour.duree.minute)

    reservations = Reserver.query.filter(Reserver.id_cl == id_client).filter(Reserver.date_cours == reservation_datetime).all()
    if len(reservations) == 0:
        #Si il a aucun cours ce jours la, il est forcement disponible
        return True
    for reservation in reservations:
        cour = get_cours_by_id(reservation.idc)
        datetime_reservations_heure_debut =  datetime(1970, 1, 1, hour=cour.heure_debut.hour, minute=cour.heure_debut.minute)
        datetime_reservations_heure_fin =  datetime(1970, 1, 1, hour=cour.heure_debut.hour, minute=cour.heure_debut.minute) + timedelta(hours=cour.duree.hour, minutes=cour.duree.minute)
        #On verifie si les dates se chevauchent
        if not (datetime_reservations_heure_fin < datetime_heure_debut or  datetime_heure_fin  < datetime_reservations_heure_debut):
            #Elles se chevauchent
            return False
    return True

    