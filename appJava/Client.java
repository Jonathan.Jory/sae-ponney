public class Client extends Personne{
    private int idp;
    private boolean cotisation;
    private int idP;
    private String nomP;
    private String prenomP;
    private int age;
    private double poids;
    private String numTel;

    public Client(int idP, String nomP, String prenomP, int age, double poids, String numTel, boolean cotisation) {
        super(idP, nomP, prenomP, age, poids, numTel);
        this.cotisation = cotisation;
    }

    public int getIdCl() {
        return this.idP;
    }
    public void setIdCl(int idp) {
        this.idP = idp;
    }
    public boolean isCotisation() {
        return cotisation;
    }
    public void setCotisation(boolean cotisation) {
        this.cotisation = cotisation;
    }

    @Override
    public String toString(){
        String res = "Le client : "+super.toString();
        if (this.cotisation){
            res += " il a payé";
        }
        else{
            res += " il n'a payé";
        }
        return res;

    }
}

