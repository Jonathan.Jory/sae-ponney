from flask import Flask
from flask_login import LoginManager
from flask_bootstrap import Bootstrap5 as Bootstrap 
from flask_sqlalchemy import SQLAlchemy





app = Flask(__name__, static_folder='static')



Bootstrap(app)
login_manager = LoginManager(app)

import os.path
def mkpath(p):
    return os.path.normpath(
        os.path.join(
        os.path.dirname( __file__ ),
        p))
app.config["SQLALCHEMY_DATABASE_URI"] = ("sqlite:///"+mkpath("../myapp.db"))
app.config["SQLALCHEMY_ENGINE_OPTIONS"] = {"encoding": "utf-8"}
app.config["SECRET_KEY"] = "550fcef2-a94c-4673-a8c3-bd315daf4726"
db = SQLAlchemy(app)

