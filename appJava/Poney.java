public class Poney {
    private int idPo;
    private String nomPo;
    private double poidSup;
    private String racePo;
    private String robe;

    public Poney(int idPo, String nomPo, double poidSup, String racePo, String robe) {
        this.idPo = idPo;
        this.nomPo = nomPo;
        this.poidSup = poidSup;
        this.racePo = racePo;
        this.robe = robe;
    }
    public int getIdPo() {
        return idPo;
    }
    public void setIdPo(int idPo) {
        this.idPo = idPo;
    }
    public String getNomPo() {
        return nomPo;
    }
    public void setNomPo(String nomPo) {
        this.nomPo = nomPo;
    }
    public double getPoidSup() {
        return poidSup;
    }
    public void setPoidSup(double poidSup) {
        this.poidSup = poidSup;
    }
    public String getRacePo() {
        return racePo;
    }
    public void setRacePo(String racePo) {
        this.racePo = racePo;
    }
    public String getRobe() {
        return robe;
    }
    public void setRobe(String robe) {
        this.robe = robe;
    }

    @Override
    public String toString(){
        return "Le poney d'id "+idPo+" "+nomPo+ "est de race "+racePo+" peut supporter "+poidSup+"kg à pour robe "+robe;
    }
    
}


