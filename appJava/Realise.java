public class Realise {
    int idPay;
    int idp;
    public Realise(int idPay, int idp) {
        this.idPay = idPay;
        this.idp = idp;
    }
    public int getIdPay() {
        return idPay;
    }
    public void setIdPay(int idPay) {
        this.idPay = idPay;
    }
    public int getIdp() {
        return idp;
    }
    public void setIdp(int idp) {
        this.idp = idp;
    }

}
