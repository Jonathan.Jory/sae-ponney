public class Moniteur extends Personne{
    private int idp;
    private int idP;
    private String nomP;
    private String prenomP;
    private int age;
    private double poids;
    private String numTel;

    public Moniteur(int idP, String nomP, String prenomP, int age, double poids, String numTel) {
        super(idP, nomP, prenomP, age, poids, numTel);
    }

    public int getIdM() {
        return idP;
    }
    public void setIdM(int idp) {
        this.idP = idp;
    }
    @Override
    public String toString(){
        String res = "Le moniteur : "+super.toString();
        return res;

    }
}

