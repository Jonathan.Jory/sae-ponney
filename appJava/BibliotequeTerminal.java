import java.sql.Date;
import java.sql.Time;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

public class BibliotequeTerminal {

    /**
     * Il affiche un menu principal, et selon le choix de l'utilisateur, il appelle un sous menu
     * 
     * @param poneyBD l'objet qui sera utilisé pour accéder à la base de données.
     */
    public static void lancerAffichage(PoneyBD poneyBD){
        boolean fini = false;
        Scanner scanner = new Scanner(System.in);
        while (!fini){
            System.out.println("------------------------------------------------");
            System.out.println("| MENU PRINCIPAL Choisissez une option         |");
            System.out.println("|----------------------------------------------|");
            System.out.println("| Option 0:    Menu INSERTION                  |");
            System.out.println("| Option 1:    Menu AFFICHER TOUS              |");
            System.out.println("| Option 2:    Menu AFFICHER UN SEUL           |");
            System.out.println("| Option 3:    Menu SUPPRIMER UN               |");
            System.out.println("| Option 4:    QUITTER                         |");
            System.out.println("------------------------------------------------");
            System.out.print("Option : ");
            int nombre = -1;
            try{
                
                nombre = scanner.nextInt();
            
                switch(nombre){
                    case 0:
                        System.out.println("");
                        BibliotequeTerminal.menuInserer(scanner, poneyBD);
                        break;
                    case 1:
                        System.out.println("");
                        BibliotequeTerminal.menuAfficherTous(scanner, poneyBD);
                        break;
                    case 2:
                        System.out.println("");
                        BibliotequeTerminal.menuAfficherUn(scanner, poneyBD);
                        break;
                    case 3:
                        System.out.println("");
                        BibliotequeTerminal.menuSupprimer(scanner, poneyBD);
                        break;
                    case 4:
                        fini = true;
                        break;
                    default:
                        System.out.println("L'option n'existe pas");
                        break;
                }      
            }
            catch(InputMismatchException e){
                scanner.nextLine();
                System.out.println("Erreur option invalide : "+e);
            }  
          }
          scanner.close();
    }



    /**
     * Il affiche un menu avec des options pour afficher toutes les données de la base de données
     * 
     * @param scanner un objet Scanner qui sera utilisé pour lire l'entrée de l'utilisateur.
     * @param poneyBD l'objet de base de données
     */
    public static void menuAfficherTous(Scanner scanner, PoneyBD poneyBD){
        boolean sortir = false;
        while(!sortir){
            System.out.println("------------------------------------------------");
            System.out.println("|          Menu Afficher Tous                  |");
            System.out.println("------------------------------------------------");
            System.out.println("| Option 0 :  afficher tous les poneys         |");
            System.out.println("| Option 1 :  afficher tous les clients        |");
            System.out.println("| Option 2 :  afficher tous les moniteurs      |");
            System.out.println("| Option 3 :  afficher tous les cours          |");
            System.out.println("| Option 4 :  afficher toutes les réservations |");
            System.out.println("| Option 5 : QUITTER                           |");
            System.out.println("------------------------------------------------");
            System.out.print("Option : ");

            int nombre = -1;
            try{
                
                nombre = scanner.nextInt();
                switch(nombre){
                    case 0:
                        poneyBD.afficherTousLesPoney();
                        break;
                    case 1:
                        poneyBD.afficherTousLesCliens();
                        break;
                    case 2:
                        poneyBD.afficherTousLesMoniteurs();
                        break;
                    case 3:
                        poneyBD.afficherTousLesCours();
                        break;
                    case 4:
                        poneyBD.afficherToutesLesReservations();
                        break;
                    case 5:
                        sortir = true;
                        break;
                    default:
                        System.out.println("L'option n'existe pas");
                        break;
                }

                if (!sortir){
                    System.out.println("");
                    System.out.println("Afficher de nouveau options ? 1 pour oui 0 pour non");
                    int continuer = scanner.nextInt();
                    if (continuer != 1){
                        sortir = true;
                    }
                }
            }
            catch(InputMismatchException e){
                scanner.nextLine();
                System.out.println("Erreur option invalide : "+e);
            } 
        }
    }

    /**
     * Il affiche un menu avec 5 options, chaque option appelle une fonction qui affiche un seul
     * élément de la base de données
     * 
     * @param scanner l'objet scanner qui sera utilisé pour lire l'entrée de l'utilisateur
     * @param poneyBD l'objet de base de données
     */
    public static void menuAfficherUn(Scanner scanner, PoneyBD poneyBD){
        boolean sortir = false;
        while(!sortir){
            System.out.println("------------------------------------------------");
            System.out.println("|            Menu Afficher Un                  |");
            System.out.println("------------------------------------------------");
            System.out.println("| Option 0 : Afficher un poney                 |");
            System.out.println("| Option 1 : Afficher un client                |");
            System.out.println("| Option 2 : Afficher un moniteur              |");
            System.out.println("| Option 3 : Afficher un cours                 |");
            System.out.println("| Option 4 : Afficher des reservations         |");
            System.out.println("| Option 5 : QUITTER                           |");
            System.out.println("------------------------------------------------");
            System.out.print("Option : ");
            int nombre = -1;
            try{
                nombre = scanner.nextInt();
            
                switch(nombre){
                    case 0:
                        afficheUnPoney(poneyBD);
                        break;
                    case 1:
                        afficheUnClient(poneyBD);
                        break;
                    case 2:
                        afficheUnMoniteur(poneyBD);
                        break;
                    case 3:
                        afficheUnCours(poneyBD);
                        break;
                    case 4:
                        afficheUneReservation(poneyBD);
                        break;
                    case 5:
                        sortir = true;
                        break;
                    default:
                        System.out.println("L'option n'existe pas");
                        break;
                }

                if (!sortir){
                    System.out.println("");
                    System.out.println("Afficher de nouveau options ? 1 pour oui 0 pour non");
                    int continuer = scanner.nextInt();
                    if (continuer != 1){
                        sortir = true;
                    }
                }
            }
            catch(InputMismatchException e){
                scanner.nextLine();
                System.out.println("Erreur option invalide : "+e);
            } 
        }



    }

    /**
     * C'est un menu qui permet à l'utilisateur de supprimer un client, un moniteur, un poney, un cours
     * ou une réservation
     * 
     * @param scanner l'objet scanner qui est utilisé pour lire l'entrée de l'utilisateur
     * @param poneyBD l'objet de base de données
     */
    public static void menuSupprimer(Scanner scanner, PoneyBD poneyBD){
        boolean sortir = false;
        while(!sortir){
            System.out.println("------------------------------------------------");
            System.out.println("|            Menu Supprimer Un                  |");
            System.out.println("------------------------------------------------");
            System.out.println("| Option 0 : supprimer un client                |");
            System.out.println("| Option 1 : supprimer un moniteur              |");
            System.out.println("| Option 2 : supprimer un poney                 |");
            System.out.println("| Option 3 : supprimer un cours                 |");
            System.out.println("| Option 4 : supprimer une reservation          |");
            System.out.println("| Option 5 : QUITTER                            |");
            System.out.println("------------------------------------------------");
            System.out.print("Option : ");

            int nombre = -1;
            try{
                
                nombre = scanner.nextInt();
                switch(nombre){
                    case 0:
                        supprimerUnClient(poneyBD);
                        break;
                    case 1:
                        supprimerUnMoniteur(poneyBD);
                        break;
                    case 2:
                        supprimerUnPoney(poneyBD);
                        break;
                    case 3:
                        supprimerUnCours(poneyBD);
                        break;
                    case 4:
                        supprimerUneReservation(poneyBD);
                        break;
                    case 5:
                        sortir = true;
                        break;
                    default:
                        System.out.println("L'option n'existe pas");
                        break;
                }

                if (!sortir){
                    System.out.println("");
                    System.out.println("Afficher de nouveau options ? 1 pour oui 0 pour non");
                    int continuer = scanner.nextInt();
                    if (continuer != 1){
                        sortir = true;
                    }
                }
            }
            catch(InputMismatchException e){
                scanner.nextLine();
                System.out.println("Erreur option invalide : "+e);
            } 
        }

    }

    /**
     * C'est un menu qui permet à l'utilisateur d'insérer une nouvelle entrée dans la base de données
     * 
     * @param scanner l'objet scanner qui sera utilisé pour lire l'entrée de l'utilisateur
     * @param poneyBD l'objet de base de données
     */
    public static void menuInserer(Scanner scanner, PoneyBD poneyBD){
        boolean sortir = false;
        while(!sortir){
            System.out.println("------------------------------------------------");
            System.out.println("|             Menu Inserer Un                  |");
            System.out.println("------------------------------------------------");
            System.out.println("| Option 0 : Ajouter un poney                  |");
            System.out.println("| Option 1 : Ajouter un client                 |");
            System.out.println("| Option 2 : Ajouter un moniteur               |");
            System.out.println("| Option 3 : Ajouter un cours                  |");
            System.out.println("| Option 4 : Ajouter une reservation           |");
            System.out.println("| Option 5 : Ajouter un payement               |");
            System.out.println("| Option 6 : QUITTER                           |");
            System.out.println("------------------------------------------------");
            System.out.print("Option : ");

            int nombre = -1;
            try{
                
                nombre = scanner.nextInt();
                switch(nombre){
                    case 0:
                        ajoutePoney(poneyBD);
                        break;
                    case 1:
                        ajouteClient(poneyBD);
                        break;
                    case 2:
                        ajouteMoniteur(poneyBD);
                        break;
                    case 3:
                        ajouteCours(poneyBD);
                        break;
                    case 4:
                        ajouteReservation(poneyBD);
                        break;
                    case 5:
                        ajoutePayement(poneyBD);
                        break;
                    case 6:
                        sortir = true;
                        break;
                    default:
                        System.out.println("L'option n'existe pas");
                        break;
                }

                if (!sortir){
                    System.out.println("");
                    System.out.println("Afficher de nouveau options ? 1 pour oui 0 pour non");
                    int continuer = scanner.nextInt();
                    if (continuer != 1){
                        sortir = true;
                    }
                }
            }
            catch(InputMismatchException e){
                scanner.nextLine();
                System.out.println("Erreur option invalide : "+e);
            } 
        }



    }
      
    /**
     * Il demande à l'utilisateur de saisir le nom, le prénom, l'âge, le poids et le numéro de
     * téléphone d'un client, puis il ajoute ce client à la base de données
     * 
     * @param poneyBD l'objet qui contient la connexion à la base de données
     */
    public static void ajouteClient(PoneyBD poneyBD){
        /*String nomP, String prenomP, int age, double poids, String numTel*/
        try {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Entrez le nom du client");
        String nom = scanner.nextLine();
        System.out.println("Entrez le prenom du client");
        String prenom = scanner.nextLine();
        System.out.println("Entrez le num de tel du client");
        String tel = scanner.nextLine();
        System.out.println("Entrez l'age du client");
        int age = scanner.nextInt();
        System.out.println("Entrez le poids du client");
        double poids = scanner.nextDouble();
        poneyBD.ajouterClient(nom, prenom, age, poids, tel);
         
    }
        catch(InputMismatchException e){
            System.out.println("Il y a eu une erreur lors de l'insertion " +e);
        }
    }

    /**
     * Il demande à l'utilisateur le nom, le prénom, l'âge, le poids et le numéro de téléphone d'un
     * nouveau moniteur, puis l'insère dans la base de données
     * 
     * @param poneyBD l'objet qui contient la connexion à la base de données
     */
    public static void ajouteMoniteur(PoneyBD poneyBD){
        /*String nomP, String prenomP, int age, double poids, String numTel*/
        try {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Entrez le nom du moniteur");
        String nom = scanner.nextLine();
        System.out.println("Entrez le prenom du moniteur");
        String prenom = scanner.nextLine();
        System.out.println("Entrez le num de tel du moniteur");
        String tel = scanner.nextLine();
        System.out.println("Entrez l'age du moniteur");
        int age = scanner.nextInt();
        System.out.println("Entrez le poids du moniteur");
        double poids = scanner.nextDouble();
        poneyBD.ajouterMoniteur(nom, prenom, age, poids, tel);
    }
        catch(InputMismatchException e){
            System.out.println("Il y a eu une erreur lors de l'insertion " +e);
        }
    }

    /**
     * Il demande à l'utilisateur d'entrer le nom, la race, la robe et le poids d'un poney, puis
     * l'insère dans la base de données
     * 
     * @param poneyBD le nom de la classe
     */
    public static void ajoutePoney(PoneyBD poneyBD){
        /*String nomPo, double poidSup, String racePo, String robe*/
        try {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Entrez le nom du poney");
        String nom = scanner.nextLine();
        System.out.println("Entrez la race du poney");
        String race = scanner.nextLine();
        System.out.println("Entrez la robe du poney");
        String robe = scanner.nextLine();
        System.out.println("Entrez le poids que le poney peut supporter");
        double poids = scanner.nextDouble();
        poneyBD.ajouterPoney(nom, poids, race, robe);
         
    }
        catch(InputMismatchException e){
            System.out.println("Il y a eu une erreur lors de l'insertion " +e);
        }
    }

    /**
     * Il demande à l'utilisateur d'entrer le type, le nom, la description et la durée d'un cours,
     * ainsi que l'identifiant de l'instructeur, puis ajoute le cours à la base de données
     * 
     * @param poneyBD la connexion à la base de données
     */
    public static void ajouteCours(PoneyBD poneyBD){
        /*int idM, String nomC, String typeC, String descriptionC, Time duree*/
        try {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Entrez le type de cours particulier/Collectif");
        String type = scanner.nextLine();
        System.out.println("Entrez le nom du cours");
        String nomCours = scanner.nextLine();
        System.out.println("Entrez la description du cours");
        String descCours = scanner.nextLine();
        System.out.println("Entrez la durée du cour sous la forme hh:mm:ss");
        String dureeCours = scanner.nextLine();    
        String[] partsDureeCours = dureeCours.split(":");
        Time dureeCoursTime = new Time(Integer.parseInt(partsDureeCours[0]), Integer.parseInt(partsDureeCours[1]), Integer.parseInt(partsDureeCours[2]));
        System.out.println("Entrez l'id du moniteur");
        int idMoniteur = scanner.nextInt();
        poneyBD.ajouterCours(idMoniteur, nomCours, type, descCours, dureeCoursTime);
         
    }
        catch(InputMismatchException e){
            System.out.println("Il y a eu une erreur lors de l'insertion " +e);
        }
    }

    /**
     * Il demande à l'utilisateur de saisir l'heure de début, la date, l'identifiant du cours,
     * l'identifiant du poney et l'identifiant du client pour la réservation, puis il insère la
     * réservation dans la base de données
     * 
     * @param poneyBD l'objet de base de données
     */
    public static void ajouteReservation(PoneyBD poneyBD){
        /*Time heureD, Date dateD, int idC, int idPo, int idCl*/
        try {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Entrez l'heure de début du cour pour cette réservation sous la forme hh:mm:ss");
        String heureDCours = scanner.nextLine();    
        String[] partsHeureDCours = heureDCours.split(":");
        Time heureDCoursTime = new Time(Integer.parseInt(partsHeureDCours[0]), Integer.parseInt(partsHeureDCours[1]), Integer.parseInt(partsHeureDCours[2]));

        System.out.println("Entrez la date du cours pour cette réservation sous la forme dd/mm/yy");
        String dateDCours = scanner.nextLine();
        String[] partsDateCours = dateDCours.split("/");  
        Date dateCoursDate = new Date(Integer.parseInt(partsDateCours[2])-1900, Integer.parseInt(partsDateCours[1])-1, Integer.parseInt(partsDateCours[0]));

        System.out.println("Entrez l'id du cours");
        int idCours = scanner.nextInt();

        System.out.println("Entrez l'id du poney");
        int idPoney = scanner.nextInt();

        System.out.println("Entrez l'id du client pour cette reservation");
        int idP = scanner.nextInt();
         
        poneyBD.ajouterReservation(heureDCoursTime, dateCoursDate, idCours, idPoney, idP);
        
    }
    
        catch(InputMismatchException e){
            System.out.println("Il y a eu une erreur lors de l'insertion " +e);
        }
        
    }


    public static void ajoutePayement(PoneyBD poneyBD){
        try {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Entrez la date du payement sous la forme dd/mm/yy");
        String datePayement = scanner.nextLine();
        String[] partsDatePayement = datePayement.split("/");  
        Date datePayementDate = new Date(Integer.parseInt(partsDatePayement[2])-1900, Integer.parseInt(partsDatePayement[1])-1, Integer.parseInt(partsDatePayement[0]));
        System.out.println("Entrez l'id du client");
        int idp = scanner.nextInt();
        System.out.println("Entrez le montant du payement");
        double montant = scanner.nextDouble();
        poneyBD.ajouterPayement(datePayementDate, montant, idp);
    }
        catch(InputMismatchException e){
            System.out.println("Il y a eu une erreur lors de l'insertion " +e);
        }
    }


    /**
     * Cette fonction est utilisée pour supprimer un client de la base de données
     * 
     * @param poneyBD l'objet de base de données
     */
    public static void supprimerUnClient(PoneyBD poneyBD){
        /*Time heureD, Date dateD, int idC, int idPo, int idCl*/
        try {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Entrez l'id du client a supprimer : ");
        System.out.println("Entrez -1 pour afficher tous les clients avec leurs id");
        int idP = scanner.nextInt();
        while (idP == -1){
            poneyBD.afficherTousLesCliens(); /* A changer */
            System.out.println("Entrez l'id du client a supprimer : ");
            idP = scanner.nextInt();
        }
        poneyBD.supprimerClient(idP);    
    }
        catch(InputMismatchException e){
            System.out.println("Il y a eu une erreur lors de la suppression " +e);
        }
        
    }

    /**
     * Il demande à l'utilisateur d'entrer l'identifiant du moniteur qu'il souhaite supprimer, puis le
     * supprime
     * 
     * @param poneyBD l'objet qui contient la connexion à la base de données
     */
    public static void supprimerUnMoniteur(PoneyBD poneyBD){
        /*Time heureD, Date dateD, int idC, int idPo, int idCl*/
        try {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Entrez l'id du Moniteur a supprimer : ");
        System.out.println("Entrez -1 pour afficher tous les Moniteur avec leurs id");
        int idP = scanner.nextInt();
        while (idP == -1){
            poneyBD.afficherTousLesMoniteurs(); /* A changer */
            System.out.println("Entrez l'id du Moniteur a supprimer : ");
            idP = scanner.nextInt();
        }
         
        poneyBD.supprimerMoniteur(idP);
        
    }
        catch(InputMismatchException e){
            System.out.println("Il y a eu une erreur lors de la suppression " +e);
        }
        
    }

    /**
     * Cette fonction permet à l'utilisateur de supprimer un poney de la base de données
     * 
     * @param poneyBD l'objet qui contient la méthode pour supprimer un poney
     */
    public static void supprimerUnPoney(PoneyBD poneyBD){
        /*Time heureD, Date dateD, int idC, int idPo, int idCl*/
        try {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Entrez l'id du Poney a supprimer : ");
        System.out.println("Entrez -1 pour afficher tous les Poney avec leurs id");
        int idPo = scanner.nextInt();
        while (idPo == -1){
            poneyBD.afficherTousLesPoney(); /* A changer */
            System.out.println("Entrez l'id du Poney a supprimer : ");
            idPo = scanner.nextInt();
        }
         
        poneyBD.supprimerPoney(idPo);
        
    }
        catch(InputMismatchException e){
            System.out.println("Il y a eu une erreur lors de la suppression " +e);
        }
        
    }

    /**
     * Il supprime un cours de la base de données.
     * 
     * @param poneyBD l'objet de base de données
     */
    public static void supprimerUnCours(PoneyBD poneyBD){
        /*Time heureD, Date dateD, int idC, int idPo, int idCl*/
        try {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Entrez l'id du Cours a supprimer : ");
        System.out.println("Entrez -1 pour afficher tous les Cours avec leurs id");
        int idCo = scanner.nextInt();
        while (idCo == -1){
            poneyBD.afficherTousLesCours(); /* A changer */
            System.out.println("Entrez l'id du Cours a supprimer : ");
            idCo = scanner.nextInt();
        }
         
        poneyBD.supprimerCours(idCo); 
    }
        catch(InputMismatchException e){
            System.out.println("Il y a eu une erreur lors de la suppression " +e);
        }
        
    }

    /**
     * Il supprime une réservation de la base de données
     * 
     * @param poneyBD la base de données
     */
    public static void supprimerUneReservation(PoneyBD poneyBD){
        /*Time heureD, Date dateD, int idC, int idPo, int idCl*/
        try {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Entrez la date de la réservation a supprimer sous la forme dd/mm/yy ");
        System.out.println("Entrez -1 pour afficher toutes les réservations avec leurs date");
        String dateReservation = scanner.nextLine();
        try{
            int nb = Integer.parseInt(dateReservation);
            while(nb == -1){
                poneyBD.afficherToutesLesReservations(); 
                System.out.println("Entrez la date de la réservation a supprimer sous la forme dd/mm/yy ");
                dateReservation = scanner.nextLine();
                nb = Integer.parseInt(dateReservation);
            }
        }
        catch(NumberFormatException e){
            ;
        }
        String[] partsDateCours = dateReservation.split("/");  
        Date dateCoursDate = new Date(Integer.parseInt(partsDateCours[2])-1900, Integer.parseInt(partsDateCours[1])-1, Integer.parseInt(partsDateCours[0]));
        System.out.println("Entrez l'heure de début de la réservation sous la forme hh:mm:ss");
        System.out.println("Entrez -1 pour afficher toutes les réservations avec leurs heures");
        String heureDCours = scanner.nextLine();  
        try{
            int nb = Integer.parseInt(heureDCours);
            while(nb == -1){
                poneyBD.afficherToutesLesReservations(); 
                System.out.println("Entrez l'heure de début de la réservation sous la forme hh:mm:ss");
                heureDCours = scanner.nextLine();
                nb = Integer.parseInt(heureDCours);
            }
        }
        catch(NumberFormatException e){
            ;
        }
        String[] partsHeureDCours = heureDCours.split(":");
        Time heureDCoursTime = new Time(Integer.parseInt(partsHeureDCours[0]), Integer.parseInt(partsHeureDCours[1]), Integer.parseInt(partsHeureDCours[2]));

        System.out.println("Entrez l'id du Client assoccié à la réservation a supprimer : ");
        System.out.println("Entrez -1 pour afficher toutes les reservations");
        int idCl = scanner.nextInt();
        while (idCl == -1){
            poneyBD.afficherToutesLesReservations(); 
            System.out.println("Entrez l'id du Client a supprimer : ");
            idCl = scanner.nextInt();
        }

        System.out.println("Entrez l'id du Cours assoccié à la réservation à supprimer : ");
        System.out.println("Entrez -1 pour afficher toutes les réservations");
        int idCo = scanner.nextInt();
        while (idCo == -1){
            poneyBD.afficherToutesLesReservations(); 
            System.out.println("Entrez l'id du Cours a supprimer : ");
            idCo = scanner.nextInt();
        }

         
        poneyBD.supprimerReservation(heureDCoursTime, dateCoursDate, idCo, idCl);
        
    }
        catch(InputMismatchException e){
            System.out.println("Il y a eu une erreur lors de la suppression " +e);
        }
        
    }



    /**
     * Cette fonction permet à l'utilisateur de saisir l'identifiant d'un client puis d'afficher les
     * informations du client
     * 
     * @param poneyBD le nom de la classe qui contient la connexion à la base de données
     */
    public static void afficheUnClient(PoneyBD poneyBD){
        /*Time heureD, Date dateD, int idC, int idPo, int idCl*/
        try {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Entrez l'id du Client a afficher : ");
        int idP = scanner.nextInt();
         
        poneyBD.afficherClientById(idP);
    }
        catch(InputMismatchException e){
            System.out.println("Il y a eu une erreur lors de la suppression " +e);
        }
        
    }

    /**
     * Cette fonction permet à l'utilisateur de saisir l'identifiant d'un moniteur et affiche les
     * informations du moniteur avec l'identifiant correspondant
     * 
     * @param poneyBD l'objet qui sera utilisé pour accéder à la base de données
     */
    public static void afficheUnMoniteur(PoneyBD poneyBD){
        /*Time heureD, Date dateD, int idC, int idPo, int idCl*/
        try {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Entrez l'id du Moniteur a afficher : ");
        int idP = scanner.nextInt();
         
        poneyBD.afficherMoniteurById(idP);
    }
        catch(InputMismatchException e){
            System.out.println("Il y a eu une erreur lors de la suppression " +e);
        }
        
    }

    /**
     * Cette fonction permet à l'utilisateur d'afficher un cours en saisissant son identifiant
     * 
     * @param poneyBD l'objet de base de données
     */
    public static void afficheUnCours(PoneyBD poneyBD){
        /*Time heureD, Date dateD, int idC, int idPo, int idCl*/
        try {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Entrez l'id du Cours a afficher : ");
        int idP = scanner.nextInt();
         
        poneyBD.afficherCoursById(idP);
    }
        catch(InputMismatchException e){
            System.out.println("Il y a eu une erreur lors de la suppression " +e);
        }
        
    }

    /**
     * Cette fonction permet à l'utilisateur de saisir l'identifiant d'un poney puis d'afficher les
     * informations du poney
     * 
     * @param poneyBD l'objet qui sera utilisé pour accéder à la base de données
     */
    public static void afficheUnPoney(PoneyBD poneyBD){
        /*Time heureD, Date dateD, int idC, int idPo, int idCl*/
        try {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Entrez l'id du Poney a afficher : ");
        int idP = scanner.nextInt();
        poneyBD.afficherPoneyById(idP);
    }
        catch(InputMismatchException e){
            System.out.println("Il y a eu une erreur lors de la suppression " +e);
        }
        
    }

    /**
     * Il affiche toutes les réservations d'une date donnée
     * 
     * @param poneyBD l'objet de base de données
     */
    public static void afficheUneReservation(PoneyBD poneyBD){
        /*Time heureD, Date dateD, int idC, int idPo, int idCl*/
        try {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Entrez la date de la réservation sous la forme dd/mm/yy");
        String dateDCours = scanner.nextLine();
        String[] partsDateCours = dateDCours.split("/");  
        Date dateCoursDate = new Date(Integer.parseInt(partsDateCours[2])-1900, Integer.parseInt(partsDateCours[1])-1, Integer.parseInt(partsDateCours[0]));

         


        poneyBD.afficherReservationsByDate(dateCoursDate);
    }
        catch(InputMismatchException e){
            System.out.println("Il y a eu une erreur lors de l'affichage " +e);
        }
        
    }
}
