/*CREATE OR REPLACE DATABASE SAE;
USE SAE;*/

drop table REALISE;
drop TABLE HISTOPAYEMENT;
drop table RESERVER;
drop table COURS;
drop table PONEY;
drop table CLIENT;
drop table MONITEUR;
drop table PERSONNE;

CREATE TABLE PERSONNE (
  idp INT(9),
  nomp VARCHAR(42),
  prenomp VARCHAR(42),
  age INT(9),
  poids DECIMAL(6,2),
  numtel VARCHAR(10),
  PRIMARY KEY (idp)
);

CREATE TABLE MONITEUR(
  idp INT(9),
  PRIMARY KEY(idp)
);

CREATE TABLE CLIENT (
  idp INT(9),
  cotisation boolean,
  PRIMARY KEY (idp)
);

CREATE TABLE COURS (
  idc INT(9),
  idp INT(9),
  nomc VARCHAR(42),
  typec VARCHAR(42),
  descriptionc VARCHAR(200),
  duree TIME(2),
  PRIMARY KEY (idc)
);

CREATE TABLE PONEY (
  idpo INT(9),
  nompo VARCHAR(42),
  poidsup DECIMAL(6,2),
  racepo VARCHAR(20),
  robe varchar(20),
  PRIMARY KEY (idpo)
);

CREATE TABLE RESERVER (
  heureD TIME(2),
  dateD DATE,
  idc INT(9),
  idpo int(9),
  idp int(9),
  PRIMARY KEY (idc, dateD, heureD, idp)
);

CREATE TABLE HISTOPAYEMENT(
    idPay int(9),
    datePayement DATE,
    montant DECIMAL(6,2),
    PRIMARY KEY (idPay)
);

CREATE TABLE REALISE(
    idPay int(9),
    idp int(9),
    PRIMARY KEY (idPay, idp)
);

alter table COURS add foreign key (idp) references MONITEUR(idp);

alter table MONITEUR add foreign key (idp) references PERSONNE(idp);

alter table CLIENT add foreign key (idp) references PERSONNE(idp);

alter table RESERVER add foreign key (idc) references COURS(idc);
alter table RESERVER add foreign key (idpo) references PONEY(idpo);
alter table RESERVER add foreign key (idp) references CLIENT(idp);

alter TABLE REALISE add foreign key (idPay) references HISTOPAYEMENT(idPay);
alter TABLE REALISE add foreign key (idp) references CLIENT(idp);



--- Listes des Triggers implémenté:

-- verifier que le poid du client < poid supporter par le poney
delimiter |
create or replace TRIGGER poidValide before insert on RESERVER for each row 
begin
    declare mes varchar(100);
    declare poidClient int(9);
    declare poidponey int(9);

    select poidsup into poidponey from PONEY where new.idpo = idpo; -- Récupère le poids supporter par le poney
    select poids into poidClient from PERSONNE where idp = new.idp; -- Récupère le poids du client

    if  (poidClient > poidponey) then -- si le client est plus lourd alors message d'erreur
        set mes = concat("Le poids du client : ", poidClient, "kg est supérieur à celui supporté par le poney : ", poidponey, "kg");
    signal SQLSTATE '45000' set MESSAGE_TEXT = mes;
    end if;
end |
delimiter ;


-- verifier que le client à bien payé la cotisation avant de s'inscrire à un cours
delimiter |
create or replace TRIGGER cotisationOk before insert on RESERVER for each row
begin
    declare mes varchar(100);
    declare paye boolean;

    select cotisation into paye from CLIENT where new.idp = idp; -- récupère le booleen

    if  (paye = "False") then -- si il est à False alors message d'erreur
        set mes = concat("Le client n'a pas payé la cotisation");
    signal SQLSTATE '45000' set MESSAGE_TEXT = mes;
    end if;
end |
delimiter ;



-- verification si une personne pas encore client à l'ajout d'une réservation
delimiter |
create or replace TRIGGER estClient before insert on RESERVER for each row
begin
    declare mes varchar(100);
    declare clientOk int(9);

    select ifnull(count(*),0) into clientOk from CLIENT where new.idp = idp; -- récure 0 si la personne n'est pas client et 1 s'il y est

    if (clientOk = 0) then -- si il n'y est pas alors message d'erreur
        set mes = concat("La personne n'est pas cliente");
    signal SQLSTATE '45000' set MESSAGE_TEXT = mes;
    end if;
end |
delimiter ;


-- verification si une personne est pas moniteur lors de la création d'un cour
delimiter |
create or replace TRIGGER estMoniteur before insert on COURS for each row
begin
    declare mes varchar(100);
    declare moniteurOk int(9);

    select ifnull(count(*),0) into moniteurOk from MONITEUR where new.idp = idp; -- récure 0 si la personne n'est pas moniteur et 1 s'il y est

    if (moniteurOk = 0) then -- si il n'y est pas alors message d'erreur
        set mes = concat("La personne n'est pas un Moniteur");
    signal SQLSTATE '45000' set MESSAGE_TEXT = mes;
    end if;
end |
delimiter ;


-- vérifie pas de chevauchement d'heure pour un poney et qu'il dispose du temps de repos si le cours a duré 2h

delimiter |
create or replace TRIGGER heureValidePoney before insert on RESERVER for each row
begin
    declare mes varchar(300);
    declare heureDeb Time;
    declare idcour int(9);
    declare idNewcour int(9);
    declare dureeC Time;
    declare dureeCNew Time;
    declare attente Time default 0;
    declare fini boolean DEFAULT false;
    declare attenteNew Time default 0;
    declare lesReservation cursor for 
        select heureD, idc from RESERVER where new.idpo = idpo and dateD = new.dateD; -- récupère les heures et les cours pour une date et un poney
    declare continue handler for not found set fini = true;
    open lesReservation;
    while not fini do
        fetch lesReservation into heureDeb, idcour; -- parcours les lignes
        if not fini then
            select duree into dureeC from COURS where idc = idcour; -- récupère la durée du cours
            select duree into dureeCNew from COURS where idc = new.idc; -- récupère la duréee du nouveau cours
            if (dureeC >= 2) then -- si la durée est supérieur ou égale à 2h ajouter un temps d'attente
                set attente = '1:00:00';
            end if;
            if (dureeCNew >= 2) then
                set attenteNew = '1:00:00';
            end if;
            -- si le nouveau cour commence en meme temps que le cour alors message d'erreur
            if (new.heureD = heureDeb) then 
                set mes = concat("Le poney n'est pas disponnible à: ", new.heureD, " Il est soit en repos soit déjà en cour");
                signal SQLSTATE '45000' set MESSAGE_TEXT = mes;
            end if;
            -- si le nouveau cour commence après un cour et finis avant ce meme cour alors message d'erreur
            if (new.heureD > heureDeb and new.heureD < heureDeb+dureeC+attente) then 
                set mes = concat("Le poney n'est pas disponnible à: ", new.heureD, " Il est soit en repos soit déjà en cour");
                signal SQLSTATE '45000' set MESSAGE_TEXT = mes;
            end if;
            -- si le nouveau cour commence avant un cour et finis après le début de ce meme cour alors message d'erreur
            if (new.heureD < heureDeb and new.heureD+dureeCNew+attenteNew > heureDeb) then
                set mes = concat("Le poney n'est pas disponnible à: ", new.heureD, " Il est soit en repos soit déjà en cour");
                signal SQLSTATE '45000' set MESSAGE_TEXT = mes;
            end if;
        end if;
    end while;
    close lesReservation;
end |
delimiter ;




-- vérifier pas de chevauchement d'heure pour un client
delimiter |
create or replace TRIGGER heureValideClient before insert on RESERVER for each row
begin
    declare mes varchar(300);
    declare heureDeb Time;
    declare idcour int(9);
    declare idNewcour int(9);
    declare dureeC Time;
    declare dureeCNew Time;
    declare fini boolean DEFAULT false;
    declare lesReservation cursor for 
        select heureD, idc from RESERVER where new.idp = idp and dateD = new.dateD; -- récupère les heures et les cours pour une date et un client
    declare continue handler for not found set fini = true;
    open lesReservation;
    while not fini do
        fetch lesReservation into heureDeb, idcour;  -- parcours les lignes
        if not fini then
            select duree into dureeC from COURS where idc = idcour; -- récupère la durée du cours
            select duree into dureeCNew from COURS where idc = new.idc; -- récupère la duréee du nouveau cours
            -- si le nouveau cour commence en meme temps que le cour alors message d'erreur
            if (new.heureD = heureDeb) then
                set mes = concat("Le Client n'est pas disponnible à: ", new.heureD, " Il est déjà en cour");
                signal SQLSTATE '45000' set MESSAGE_TEXT = mes;
            end if;
            -- si le nouveau cour commence après un cour et finis avant ce meme cour alors message d'erreur
            if (new.heureD > heureDeb and new.heureD < heureDeb+dureeC) then
                set mes = concat("Le Client n'est pas disponnible à: ", new.heureD, " Il est déjà en cour");
                signal SQLSTATE '45000' set MESSAGE_TEXT = mes;
            end if;
             -- si le nouveau cour commence avant un cour et finis après le début de ce meme cour alors message d'erreur
            if (new.heureD < heureDeb and new.heureD + dureeCNew > heureDeb) then
                set mes = concat("Le Client n'est pas disponnible à: ", new.heureD, " Il est déjà en cour");
                signal SQLSTATE '45000' set MESSAGE_TEXT = mes;
            end if;
        end if;
    end while;
    close lesReservation;
end |
delimiter ;

