insert into PONEY(idpo, nompo, poidsup, racepo, robe) values
    (1, "Kylian", 50, "Shetland", "Café au lait"),
    (2, "Esteban", 45, "Pottok", "Chocolat"),
    (3, "Tina", 60, "Haflinger", "Alezan pie"),
    (4, "Orianne", 80, "Connemara", "Blanc"),
    (5, "Max", 65, "Shetland", "Bai");

insert into PERSONNE(idp, nomp, prenomp, age, poids, numtel) values
    (1, "De-Nardi", "Lenny", 18, 50, "0656588554"),
    (2, "Faucher", "Thomas", 19, 48, "0726354899"),
    (3, "Doudeau", "Luis", 18, 65, "0636352649"),
    (4, "Charpentier", "Maxym", 19, 63, "0660485947"),
    (5, "Jory", "Jonathan", 19, 70, "0609316756"),
    (6, "El-Mecheta", "Ayman", 19, 52, "0706054521"),
    (7, "Passelande", "Jéhanne", 19, 56, "0731459546"),
    (8, "Delalande", "Enora", 18, 46, "0712235648"),
    (9, "Aucordier", "Justine", 20, 37, "0763365025"),
    (10, "Soulas", "Emma", 19, 43, "0712354981"),
    (11, "Recoura", "Avalone", 21, 37, "0603040180"),
    (12, "Trome", "Carle", 22, 71.6, "0710365974"),
    (13, "Rally", "Martin", 23, 74.1, "0790236584"),
    (14, "Marlet", "Bob", 22, 56.3, "0730265948"),
    (15, "Jian", "Romain", 26, 85.3, "0645963276"),
    (16, "Nard", "Maria", 20, 80.3, "0785423165"),
    (17, "Morg", "Oriane", 13, 48.0, "0725451325"),
    (18, "Renaud", "Paul", 21, 62.6, "0653895414"),
    (19, "Colmar", "Jean", 15, 52.3, "0653895414"),
    (20, "Panou", "Mathias", 19, 52.6, "0621235468"),
    (21, "Bari", "Tom", 21, 63.0, "0745236581"),
    (22, "Strone", "Jacque", 18, 51.0, "0647523165"),
    (23, "Split", "Greg", 16, 48.9, "0652315468"),
    (24, "Pronet", "Vincent", 20, 57.3, "0600351832"),
    (25, "Marpre", "Nil", 14, 56.0, "0751256397"),
    (26, "Faille", "Lou", 15, 51.2, "0714230023");


insert into CLIENT(idp, cotisation) values
    (4, true),
    (6, true),
    (8, true),
    (9, true),
    (10, false),
    (11, true),
    (12, true),
    (13, false),
    (14, true),
    (15, false),
    (16, false),
    (17, true),
    (18, true),
    (19, true),
    (20, true);

insert into MONITEUR(idp) values
    (1),
    (2),
    (3),
    (5),
    (7);

insert into COURS(idc, idp, nomc, typec, descriptionc, duree) values
    (1, 1, "balade", "individuel", "balade a deux", '02:00'),
    (2, 3, "initiation", "collectif", "base de l'équitation", '02:00'),
    (3, 5, "premier galop", "collectif", "initiation galop", '02:00'),
    (4, 7, "chronometre", "individuel", "course", '02:00'),
    (5, 3, "duel", "individuel", "duel contre moniteur", '02:00');

insert into RESERVER(heureD, dateD, idc, idpo, idp) values
    ('09:00','2022-09-28', 1, 1, 8),
    ('09:00','2022-09-28', 1, 2, 9),
    ('14:00','2022-09-28', 1, 1, 11),
    ('14:00','2022-09-28', 1, 3, 14),
    ('08:00','2022-10-03', 1, 4, 19),
    ('08:00','2022-10-03', 1, 5, 20);

insert into HISTOPAYEMENT(idPay, datePayement, montant) values
    (1, '2022-09-27', 150);

insert into REALISE (idPay, idp) values
    (1, 4),
    (1, 6),
    (1, 8),
    (1, 9),
    (1, 11),
    (1, 12),
    (1, 14),
    (1, 17),
    (1, 18),
    (1, 19),
    (1, 20);
-- Insertions pour tester les triggers:

-- Trigger poidValide:
-- poid max de idpo2 = 45 < poid idp 4 = 63
insert into RESERVER(heureD, dateD, idc, idpo, idp) values (9,'2022-09-28', 1, 2, 4);

-- Trigger cotisationOk:
-- cotisation idp 13 = false
insert into RESERVER(heureD, dateD, idc, idpo, idp) values (10,'2022-09-28', 1, 4, 13);

-- Trigger estClient:
-- idp 45 = pas dans la table client
insert into RESERVER(heureD, dateD, idc, idpo, idp) values (10,'2022-09-28', 1, 4, 45);

-- Trigger estMoniteur:
-- idp 6 = pas dans la table Moniteur
insert into COURS(idc, idp, nomc, typec, descriptionc, duree) values (1,6,"test", "test","test",'02:00');

-- Trigger heureValidePoney:
-- heure idc 1 = 2, idpo 5 à déja un cours de reserver à 8h
insert into RESERVER(heureD, dateD, idc, idpo, idp) values ('7:00','2022-10-03', 1, 5, 11);

-- Trigger heureValideClient:
-- heure idc 1 = 2, idp 20 à déja un cours de reserver à 8h
insert into RESERVER(heureD, dateD, idc, idpo, idp) values ('7:30','2022-10-03', 1, 4, 20);