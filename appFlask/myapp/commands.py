import click
from .app import app , db
from datetime import datetime, time
import codecs

@app.cli.command()
def loaddb():
    """Creates the tables and populates them with data. """
    # création de toutes les tables
    db.create_all()

    # chargement de notre jeu de données
    import yaml
    dataPoney = yaml.safe_load(open("myapp/static/dataPon.yml"))


    # import des modèles
    from .models import Poney
    #première passe : création de tous les poneys
    for p in dataPoney :
        id = p["idpo"]
        nom = p["nompo"]
        poidsup = p["poidsup"]
        racepo = p["racepo"]
        robe = p["robe"]
        image = p["image"]
        o = Poney(idpo=id, nom_poney=nom, poidssup= poidsup, race= racepo, robe= robe, image=image)# attention image obligatoire
        db.session.add(o)

    from .models import Personne

    with codecs.open("myapp/static/dataPers.yml", "r", "utf-8") as f:
        dataPersonne = yaml.safe_load(f)
    #dataPersonne = yaml.safe_load(open("myapp/static/dataPers.yml"))
    for p in dataPersonne :
        
        id = p["idp"]
        nom = p["nomp"]
        prenom = p["prenomp"]
        poids = p["poids"]
        num_tel = p["numtel"]
        age = p["age"]
        password = p["password"]
        o = Personne(nom_personne=nom, prenom_personne=prenom, poids=poids, num_tel=num_tel, age_personne=age, password=password)
        db.session.add(o)

    from .models import Client
    
    with codecs.open("myapp/static/dataCl.yml", "r", "utf-8") as f:
        dataClient = yaml.safe_load(f)
    for p in dataClient :
        id = p["idp"]
        a_paye = p["cotisation"]
        o = Client(id=id, cotisation=a_paye)
        db.session.add(o)

    from .models import Moniteur
    with codecs.open("myapp/static/dataMoni.yml", "r", "utf-8") as f:
        dataMoni = yaml.safe_load(f)
    for p in dataMoni :
        id = p["idp"]
        o = Moniteur(id=id)
        db.session.add(o)
    db.session.commit()

    from .models import Cours
    with codecs.open("myapp/static/dataCours.yml", "r", "utf-8") as f:
        dataCours = yaml.safe_load(f)
    for c in dataCours :
        id_c = c["idc"]
        id_p = c["idp"]
        nom_c = c["nomc"]
        type_c = c["typec"]
        description_c = c["descriptionc"]
        duree_c = c["duree"]
        duree_c = time(int(duree_c[0:2]), int(duree_c[3:5],0))
        jours_semaine = c["jours_semaine"]
        heure_debut = c["heure_debut"]
        heure_debut = time(int(heure_debut[0:2]), int(heure_debut[3:5],0))
        o = Cours(idc= id_c, nomc = nom_c, descc = description_c, typec = type_c, duree = duree_c, id_m = id_p, jours_semaine=jours_semaine,heure_debut=heure_debut)
        db.session.add(o)
    db.session.commit()

    from .models import Reserver
    with codecs.open("myapp/static/dataReserv.yml", "r", "utf-8") as f:
        dataReserv = yaml.safe_load(f)
    for r in dataReserv :
        date_cours = r["date_cours"]
        id_c = r["idc"]
        id_po = r["idpo"]
        id_p = r["idp"]
        date_obj = datetime.strptime(date_cours, '%Y-%m-%d')
        date_cours = date_obj.date()
        o = Reserver(date_cours=date_cours, id_cl=id_p, idc=id_c,idpo=id_po)
        db.session.add(o)
    db.session.commit()
    from .models import Histo_paiement
    with codecs.open("myapp/static/dataPaiement.yml", "r", "utf-8") as f:
        dataPaiement = yaml.safe_load(f)
    for r in dataPaiement :
        prix = r["prix"]
        date_paiement = r["date"]
        idcl = r["idp"]
        date_obj = datetime.strptime(date_paiement, '%Y-%m-%d')
        date_paiement = date_obj.date()
        o = Histo_paiement(date=date_paiement, idcl=idcl,prix=prix)
        db.session.add(o)
    db.session.commit()


@app.cli.command()
def syncdb():
    """Creates all missing tables ."""
    db.create_all()
